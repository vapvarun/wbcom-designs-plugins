jQuery(document).ready(function(){
	/*jQuery(document).on('click', '.posttype-checkbox', function(){
		var posttype = jQuery(this).val();
		if(jQuery(this).is(':checked')) {
			jQuery('#posttype-label-'+posttype).show();
		} else {
			jQuery('#posttype-label-'+posttype).hide();
		}
	});*/

	jQuery(document).on('click', '#save-itmbk-general-settings', function(){
		jQuery( '#save-itmbk-general-settings' ).addClass('itmbk-btn-ajax');
        jQuery( '.ajax-spinner' ).show();
		var posttypes = [];
		var postslugs = [];
		var posttexts = [];
		jQuery('.posttype-checkbox:checked').each(function(){
			if( jQuery( this ).is( ':checked' ) ) {
				posttypes.push( jQuery( this ).val() );
				postslugs.push( jQuery( this ).data( 'cptslug' ) );
				posttexts.push( jQuery( '#posttype-label-'+jQuery( this ).val() ).val() );
			}
		});
		jQuery.post(
			ajaxurl,
			{
				'action' : 'itmbk_save_general_settings',
				'post_types' : posttypes,
				'post_slugs' : postslugs,
				'post_texts' : posttexts,
				'add_collection_btn_txt' : jQuery( '#add-collection-btn-txt' ).val(),
				'rem_collection_btn_txt' : jQuery( '#rem-collection-btn-txt' ).val(),
			},
			function(response){
				jQuery( '#save-itmbk-general-settings' ).removeClass('itmbk-btn-ajax');
		        jQuery( '.ajax-spinner' ).hide();
		        jQuery( '#itmbk_settings_save_notice' ).fadeIn('slow').delay(10000);
                jQuery( '#itmbk_settings_save_notice' ).fadeOut('slow');
			}
		);
	});
});