<?php 
//List of registered post types
$post_types = get_post_types( false, 'objects' );
$spinner_src = includes_url().'images/spinner.gif';

//Get Saved Settings
$saved_post_types = $saved_post_slugs = $saved_post_texts = $plural_post_texts = array();
$add_collection_btn_txt = $rem_collection_btn_txt = '';
$itmbk_general = unserialize( get_option( 'itmbk_general_settings' ) );

if( isset( $itmbk_general['post_types'] ) ) {
	foreach( $itmbk_general['post_types'] as $posttype ) {
		$saved_post_types[] = $posttype['type'];
		$saved_post_slugs[] = $posttype['slug'];
		$saved_post_texts[] = $posttype['text'];
		$plural_post_texts[] = getPluralPrase( $posttype['text'] );
	}
}

?>
<div id="wpbody" role="main">
	<div id="wpbody-content" aria-label="Main content" tabindex="0">
		<div class="wrap">
			<h3> Items Bookmark General Settings</h3>

			<div id="itmbk_settings_save_notice" class="updated settings-error notice">
				<p><strong>General Settings Saved.</strong></p>
			</div>

			<form method="post" action="" novalidate="novalidate">
			<span><i>Note: Please enter the singular names of custom post types.</i></span>
				<table class="form-table">
					<tbody>
						<!-- INCLUDE POST TYPES -->
						<tr>
							<th scope="row">
								<label for="blogname">Include Post Types</label>
							</th>
							<td>
								<?php foreach( $post_types as $index => $post_type) {?>
									<?php if( !in_array( $post_type->label, $plural_post_texts ) ) {?>
										<p>
											<input type="checkbox" class="posttype-checkbox" data-cptslug="<?php echo 'cpt_'.$post_type->name;?>" value="<?php echo $post_type->name;?>" <?php if( !empty( $saved_post_types ) && in_array($post_type->name, $saved_post_types ) ) echo 'checked="checked"';?>>
											<?php echo $post_type->label;?>

											<?php 
											$val = '';
											if( in_array($post_type->name, $saved_post_types) ) {
												$key = array_search($post_type->name, $saved_post_types);
												$val = $saved_post_texts[$key];
											}
											?>

											<input type="text" placeholder="Post Type Label" class="posttype-label" name="posttype-label" id="posttype-label-<?php echo $post_type->name;?>" value="<?php echo $val;?>">
										</p>
									<?php }?>
								<?php }?>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit">
					<input id="save-itmbk-general-settings" class="button button-primary" value="Save Settings" type="button">
					<img src="<?php echo $spinner_src;?>" class="ajax-spinner" alt="Spinner">
				</p>
			</form>
		</div>
		<div class="clear"></div>
	</div><!-- wpbody-content -->
	<div class="clear"></div>
</div>