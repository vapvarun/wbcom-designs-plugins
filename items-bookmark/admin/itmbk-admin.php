<?php
//Creating menu page - items bookmark admin options
add_action('admin_menu', 'itmbk_admin_page');
function itmbk_admin_page() {
	add_menu_page( 'Items Bookmark Options', 'Items Bookmark', 'manage_options', 'itmbk-options', 'itmbk_options_page', 'dashicons-star-filled', 3 );
}
function itmbk_options_page() {
	include 'itmbk-general-settings.php';
}