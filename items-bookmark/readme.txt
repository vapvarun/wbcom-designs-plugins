=== Items Bookmark ===
Contributors: wbcomdesigns
Donate link: https://wbcomdesigns.com/contact/
Tags: buddypress, cpt, bookmark
Requires at least: 3.0.1
Tested up to: 4.6.1
Stable tag: 4.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
 
This plugin allows you to make the WP posts as your favourite according to the post types. You need to create CPTs of the posts you wish to make favourite and then goto single view of any post to add them as favourite.
 
== Installation ==
 
1. Upload the entire items-bookmark folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
 
== Frequently Asked Questions ==
 
== Screenshots ==

The screenshots are present in the root of the plugin folder.
1. screenshot-1 - is the screen that shows the list of the posts that are marked fav. under any collection.
2. screenshot-2 - is the screen that shows the admin settings for the plugin.
3. screenshot-3 - is the screen that shows the add-as-fav button to the specific post type.
4. screenshot-4 - is the screen that show the block where we add any post as favourite.