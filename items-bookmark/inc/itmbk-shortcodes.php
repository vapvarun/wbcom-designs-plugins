<?php
/**
 * This is the list of the bookmarks created
 * 1. [my_bookmarks] : lists all the post for the currently logged in user
 * 2. [users_who_bookmarked] : lists all the users who have bookmarked any single post
 * 3. [post_bookmarks_count] : will take post id as parameter and will show the count of the bookmarked posts
 */

/************************************ [my_bookmarks] ************************************/
function itmbk_my_bookmarks_shortcode() {
	include 'shortcodes/my-bookmarks.php';
}
add_shortcode('my_bookmarks', 'itmbk_my_bookmarks_shortcode');

/********************** [users-who-bookmarked] ******************************************/
function itmbk_users_who_bookmarked_shortcode() {
	include 'shortcodes/users-who-bookmarked.php';
}
add_shortcode('users_who_bookmarked', 'itmbk_users_who_bookmarked_shortcode');

/******************************** [post-bookmarks-count] ********************************/
function itmbk_post_bookmarks_count_shortcode( $atts = [], $content = null ) {
	$postid = $atts['postid'];
	$postids = unserialize( get_post_meta( $postid, 'bookmarked_post_ids', true ) );
	if( !empty( $postids ) ) {
		$postids = array_unique( $postids );
		echo count( $postids );
	} else {
		echo 0;
	}
}
add_shortcode('post_bookmarks_count', 'itmbk_post_bookmarks_count_shortcode');