<?php 
//Register Custom Variables & Scripts
add_action('wp_enqueue_scripts', 'dealershout_custom_variables');
function dealershout_custom_variables() {
	wp_enqueue_script('itmbk-js-front',ITEMS_BOOKMARK_PLUGIN_URL.'assets/js/itmbk-front.js', array('jquery'));
	wp_localize_script( 'itmbk-js-front', 'ajax_object',array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_style('itmbk-front-css', ITEMS_BOOKMARK_PLUGIN_URL.'assets/css/itmbk-front.css');

	//Select2 JS & CSS
  	wp_enqueue_script( 'itmbk-select2-js', ITEMS_BOOKMARK_PLUGIN_URL.'admin/assets/js/select2.js', array( 'jquery' ) );
  	wp_enqueue_style( 'itmbk-select2-css', ITEMS_BOOKMARK_PLUGIN_URL.'admin/assets/css/select2.css' );

	//Font Awesome
	wp_enqueue_style('itmbk-font-awesome-css', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
	
}

//Register Custom Variables & Scripts for admin options
add_action('admin_init', 'itmbk_custom_variables');
function itmbk_custom_variables() {
	if( isset( $_GET['page'] ) && ( $_GET['page'] == 'itmbk-options' ) ) {
		wp_enqueue_script( 'itmbk-js-admin', ITEMS_BOOKMARK_PLUGIN_URL.'admin/assets/js/itmbk-admin.js', array( 'jquery' ) );
	  	wp_enqueue_style( 'itmbk-css-admin', ITEMS_BOOKMARK_PLUGIN_URL.'admin/assets/css/itmbk-admin.css' );

	  	//Select2 JS & CSS
	  	wp_enqueue_script( 'itmbk-select2-js', ITEMS_BOOKMARK_PLUGIN_URL.'admin/assets/js/select2.js', array( 'jquery' ) );
	  	wp_enqueue_style( 'itmbk-select2-css', ITEMS_BOOKMARK_PLUGIN_URL.'admin/assets/css/select2.css' );
	}
}