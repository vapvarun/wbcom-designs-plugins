<?php 
//Get Saved Post Types
$itmbk_general = unserialize( get_option( 'itmbk_general_settings' ) );
$flag = 0;
if( isset( $itmbk_general['post_types'] ) ) {
	$post_types = $itmbk_general['post_types'];
	$flag = 1;
}

if( $flag == 1 ) {
	add_action( 'init', 'create_cpts' );
	function create_cpts() {
		$itmbk_general = unserialize( get_option( 'itmbk_general_settings' ) );
		$post_types = $itmbk_general['post_types'];
		foreach( $post_types as $index => $post_type ) {
			$labels = array(
				'name' => getPluralPrase( $post_type['text'] ),
				'singular_name' => $post_type['text'],
				'menu_name' => getPluralPrase( $post_type['text'] ),
				'name_admin_bar' => $post_type['text'],
				'add_new' => 'Add New '.$post_type['text'],
				'add_new_item' => 'Add New '.$post_type['text'],
				'new_item' => 'New '.$post_type['text'],
				'view_item' => 'View '.$post_type['text'],
				'all_items' => 'All '.getPluralPrase( $post_type['text'] ),
				'search_items' => 'Search '.getPluralPrase( $post_type['text'] ),
				'parent_item_colon' => 'Parent '.$post_type['text'].':',
				'not_found' => 'No '.getPluralPrase( $post_type['text'] ).' Found',
				'not_found_in_trash' => 'No '.getPluralPrase( $post_type['text'] ).' Found In Trash',
			);
			$args = array(
				'labels' => $labels,
				'public' => true,
				//'menu_icon' => '',
				'publicly_queryable' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => $post_type['slug'], 'with_front' => false ),
				'capability_type' => 'post',
				'has_archive' => true,
				'hierarchical' => false,
				'menu_position' => null,
				'supports' => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( $post_type['slug'], $args );
		}
	}
}