<?php
//Save Admin General Settings
add_action( 'wp_ajax_itmbk_save_general_settings', 'itmbk_save_general_settings');
add_action( 'wp_ajax_nopriv_itmbk_save_general_settings', 'itmbk_save_general_settings');
function itmbk_save_general_settings() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'itmbk_save_general_settings' ) {
		if( isset( $_POST['post_types'] ) ) {
			$posts = array();
			foreach( $_POST['post_types'] as $index => $posttype ) {
				$posts[] = array(
					'type' => $posttype,
					'slug' => $_POST['post_slugs'][$index],
					'text' => $_POST['post_texts'][$index],
				);
			}
			$itmbk_general['post_types'] = $posts;
		}

		if( empty( $itmbk_general ) ) {
			update_option( 'itmbk_general_settings', '' );
		} else {
			update_option( 'itmbk_general_settings', serialize( $itmbk_general ) );
		}
		echo 'save-success';
		die;
	}
}

//Add Post To New Collection
add_action( 'wp_ajax_itmbk_add_post_to_new_collection', 'itmbk_add_post_to_new_collection');
add_action( 'wp_ajax_nopriv_itmbk_add_post_to_new_collection', 'itmbk_add_post_to_new_collection');
function itmbk_add_post_to_new_collection() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'itmbk_add_post_to_new_collection' ) {
		$post_id = $_POST['post_id'];
		$collection_name = $_POST['collection_name'];
		$add_to_cpt = $_POST['add_to_cpt'];
		
		//Create New Collection
		$args = array(
			'post_type' => $add_to_cpt,
			'post_title' => $collection_name,
			'post_status' => 'publish',
			'post_name' => strtolower( str_replace( ' ', '-', $collection_name ) ),
		);
		$collection_id = wp_insert_post( $args );

		//Update post id in collection meta
		$post_ids[] = $post_id;
		update_post_meta( $collection_id, 'bookmarked_post_ids', serialize( $post_ids ) );

		//Update collection id in post meta
		$collection_ids = get_post_meta( $post_id, 'bookmarked_in', true );
		if( empty( $collection_ids ) ) {
			$collection_ids[] = $collection_id;
			update_post_meta( $post_id, 'bookmarked_in', serialize( $collection_ids ) );
		} else {
			$collection_ids = unserialize( $collection_ids );
			$collection_ids[] = $collection_id;
			update_post_meta( $post_id, 'bookmarked_in', serialize( $collection_ids ) );
		}

		//Update post id in user meta
		$userid = get_current_user_id();
		$post_ids = get_user_meta( $userid, 'bookmarked_posts', true );
		if( empty( $post_ids ) ) {
			$post_ids[] = $post_id;
			update_user_meta( $userid, 'bookmarked_posts', serialize( $post_ids ) );
		} else {
			$post_ids = unserialize( $post_ids );
			$post_ids[] = $post_id;
			update_user_meta( $userid, 'bookmarked_posts', serialize( $post_ids ) );
		}

		echo 'added-to-new-collection-success';
		die;
	}
}

//Add Post To Collection
add_action( 'wp_ajax_itmbk_add_post_to_collection', 'prefix_ajax_itmbk_add_post_to_collection');
add_action( 'wp_ajax_nopriv_itmbk_add_post_to_collection', 'prefix_ajax_itmbk_add_post_to_collection');
function prefix_ajax_itmbk_add_post_to_collection() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'itmbk_add_post_to_collection' ) {
		$post_id = $_POST['post_id'];
		$collection_id = $_POST['collection_id'];

		//Update post id in collection meta
		$post_ids = get_post_meta( $collection_id, 'bookmarked_post_ids', true );
		if( empty( $post_ids ) ) {
			$post_ids[] = $post_id;
			update_post_meta( $collection_id, 'bookmarked_post_ids', serialize( $post_ids ) );
		} else {
			$post_ids = unserialize( $post_ids );
			//Check if post is already added to the selected collection for the currently logged in user
			$userid = get_current_user_id();
			$user_bookmarked_post_ids = unserialize( get_user_meta( $userid, 'bookmarked_posts', true ) );
			if( isset( $user_bookmarked_post_ids ) && in_array( $post_id, $user_bookmarked_post_ids ) ) {
				echo 'already-added';
				die;
			}
			$post_ids[] = $post_id;
			update_post_meta( $collection_id, 'bookmarked_post_ids', serialize( $post_ids ) );
		}

		//Update collection id in post meta
		$collection_ids = get_post_meta( $post_id, 'bookmarked_in', true );
		if( empty( $collection_ids ) ) {
			$collection_ids[] = $collection_id;
			update_post_meta( $post_id, 'bookmarked_in', serialize( $collection_ids ) );
		} else {
			$collection_ids = unserialize( $collection_ids );
			$collection_ids[] = $collection_id;
			update_post_meta( $post_id, 'bookmarked_in', serialize( $collection_ids ) );
		}

		//Update post id in user meta
		$userid = get_current_user_id();
		$post_ids = get_user_meta( $userid, 'bookmarked_posts', true );
		if( empty( $post_ids ) ) {
			$post_ids[] = $post_id;
			update_user_meta( $userid, 'bookmarked_posts', serialize( $post_ids ) );
		} else {
			$post_ids = unserialize( $post_ids );
			$post_ids[] = $post_id;
			update_user_meta( $userid, 'bookmarked_posts', serialize( $post_ids ) );
		}

		echo 'added-to-collection-success';
	    die;
	}
}

//User Login
add_action( 'wp_ajax_itmbk_user_login', 'itmbk_user_login');
add_action( 'wp_ajax_nopriv_itmbk_user_login', 'itmbk_user_login');
function itmbk_user_login() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'itmbk_user_login' ) {
		$credentials['user_login'] = $_POST['username'];
		$credentials['user_password'] = $_POST['password'];
		$credentials['remember'] = true;
		$secure_cookie = true;
		$user = wp_signon( $credentials, $secure_cookie );
		if( is_wp_error( $user ) ) {
			//echo $user->get_error_message();
			echo 'login-fail';
		} else {
			echo 'login-success';
		}
		die;
	}
}

//User Signup
add_action( 'wp_ajax_itmbk_user_signup', 'itmbk_user_signup');
add_action( 'wp_ajax_nopriv_itmbk_user_signup', 'itmbk_user_signup');
function itmbk_user_signup() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'itmbk_user_signup' ) {
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$userid = username_exists($username);
		if(!$userid and email_exists($email) == false) {
			//$random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
			$user_id = wp_create_user($username, $password, $email);
			//wp_update_user(array('ID' => $userid, 'display_name' => $name));
			
			$credentials = array();
			$credentials['user_login'] = $username;
			$credentials['user_password'] = $password;
			$credentials['remember'] = false;
			$secure_cookie = false;
			$user = wp_signon($credentials, $secure_cookie);
			echo "signup-success";
		} else {
			echo "user-exists";
		}
		die;
	}
}

//Removed Post From My Bookmarks
add_action( 'wp_ajax_itmbk_remove_from_mypost', 'itmbk_remove_from_mypost');
add_action( 'wp_ajax_nopriv_itmbk_remove_from_mypost', 'itmbk_remove_from_mypost');
function itmbk_remove_from_mypost() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'itmbk_remove_from_mypost' ) {
		$remove_post = $_POST['remove_post'];
		$remove_from_post = $_POST['remove_from_post'];

		$bookmarked_posts = unserialize( get_post_meta( $remove_from_post, 'bookmarked_post_ids', true ) );
		foreach( $bookmarked_posts as $key => $bookmarked_post ) {
			if( $bookmarked_post == $remove_post ) {
				//unset( $bookmarked_posts[$key] );
			}
		}

		if( empty( $bookmarked_posts ) ) {
			update_post_meta( $remove_from_post, 'bookmarked_post_ids', '' );
			echo 'no-bookmarked-posts';
		} else {
			$bookmarked_posts = array_unique( $bookmarked_posts );
			update_post_meta( $remove_from_post, 'bookmarked_post_ids', serialize( $bookmarked_posts ) );
			foreach( $bookmarked_posts as $bookmarked_posts ) {
				$post = get_post( $bookmarked_posts );
				$arr[] = array(
					'id' => $post->ID,
					'title' => $post->post_title,
					'permalink' => get_permalink( $post->ID ),
				);
			}
			echo json_encode( $arr );
		}
		die;
	}
}