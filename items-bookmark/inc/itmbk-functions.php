<?php
//Get plural words
function getPluralPrase( $phrase, $value = 2 ) {
    $plural='';
    if($value>1){
        for($i=0;$i<strlen($phrase);$i++){
            if($i==strlen($phrase)-1){
                $plural.=($phrase[$i]=='y')? 'ies':(($phrase[$i]=='s'|| $phrase[$i]=='x' || $phrase[$i]=='z' || $phrase[$i]=='ch' || $phrase[$i]=='sh')? $phrase[$i].'es' :$phrase[$i].'s');
            }else{
                $plural.=$phrase[$i];
            }
        }
        return $plural;
    }
    return $phrase;
}

//Add Fav BUtton After Post Content
add_filter( 'the_content', 'add_favourite_after_content', 10, 1 );
function add_favourite_after_content( $content ) {
    global $post;
    $flag = 1;
    $post_type = $post->post_type;

    //Get Saved Settings
    $itmbk_general = unserialize( get_option( 'itmbk_general_settings' ) );
    if( isset( $itmbk_general['post_types'] ) ) {
        foreach( $itmbk_general['post_types'] as $posttype ) {
            $saved_post_types[] = $posttype['type'];
            $saved_post_slugs[] = $posttype['slug'];
            $saved_post_texts[] = $posttype['text'];
        }
    }

    //Hide Add-Bookmark button on [my_bookmarks]
    if( is_page() ) {
        global $post;
        $bookmarks = array(
            '[my_bookmarks]',
            '[users_who_bookmarked]',
        );
        
        if( in_array( $post->post_content, $bookmarks ) ) {
            $flag = 0;
        }
    }
    
    if( $flag == 1 ) {
        if( in_array( $post_type, $saved_post_types ) ) {
            $add_to_cpt = 'cpt_'.$post_type;
            $postObj = get_post_type_object( $add_to_cpt );
            //echo '<pre>'; print_r( $postObj ); die;
            $post_id = $post->ID;

            $btn_txt = "Add To ".$postObj->label;

            //Gather all collections
            $args = array(
                'post_type' => $add_to_cpt,
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'author' => get_current_user_id(),
            );
            $posts = new WP_Query( $args );
            $collections = $posts->posts;
            //print_r( $collections ); die;

            $fav_html = '';
            $fav_html .= '<a data-postid="'.$post_id.'" href="#addBookmark" id="add_bookmark" class="add_to_bookmark_btn">'.$btn_txt.'</a>';
            $fav_html .= '<div id="addBookmark" class="modalDialog">';
            $fav_html .= '<div>';
            $fav_html .= '<a href="#close" title="Close" class="close">X</a>';
            $fav_html .= '<div class="full_width">';
            
            if( is_user_logged_in() ) {
                $fav_html .= '<span class="error_collection_select">Select Appropriate Collection</span>';
                $fav_html .= '<span class="already_added">Already Added In This Playlist</span>';
                $fav_html .= '<span class="add_collection_success">Post Added To Collection</span>';
                $fav_html .= '<div class="me_bookmark">';
                $fav_html .= '<span>Bookmark <b>Me</b></span>';
                $fav_html .= '</div>';
                $fav_html .= '<div class="me_bookmark">';
                $fav_html .= '<form>';
                $fav_html .= '<select class="select_width" id="collections-list">';
                $fav_html .= '<option value="0">--Select--</option>';
                foreach( $collections as $collection ) {
                    $fav_html .= '<option value="'.$collection->ID.'">'.$collection->post_title.'</option>';
                }
                $fav_html .= '</select>';
                $fav_html .= '</form>';
                $fav_html .= '</div>';
                $fav_html .= '<div class="me_bookmark">';
                $fav_html .= '<button id="add-to-collection" data-add_to_cpt="'.$add_to_cpt.'" class="button button5">ADD TO COLLECTION</button>';
                $fav_html .= '</div>';
                $fav_html .= '<div class="me_bookmark">';
                $fav_html .= '<input type="text" placeholder="Collection Name" id="new-collection-name">';
                $fav_html .= '<button class="button button4" id="new-collection-btn">NEW COLLECTION</button>';
                $fav_html .= '<button class="button button4" data-add_to_cpt="'.$add_to_cpt.'" id="add-to-new-collection-btn">ADD TO NEW COLLECTION</button>';
                $fav_html .= '</div>';
            } else {

                $fav_html .= '<p>Please login before you bookmark this post.</p>';
                $fav_html .= '<div class="user-login-box">';
                $fav_html .= '<span class="error_failed_login">Login Failed. Please Try Again.</span>';
                $fav_html .= '<span class="success_login">Login Success. Redirecting....</span>';
                $fav_html .= '<input type="text" placeholder="Your Username" id="itmbk-login-username" required>';
                $fav_html .= '<input type="password" placeholder="Your Password" id="itmbk-login-password" required>';
                $fav_html .= '<input type="button" class="button button4" value="Login" id="itmbk-login">';
                $fav_html .= '<p class="not-member">';
                $fav_html .= 'Not a member?';
                $fav_html .= '<a id="itmbk-click-to-signup" href="javascript:void(0);" title="Register for this site">Register</a> here.';
                $fav_html .= '</p>';
                $fav_html .= '</div>';

                $fav_html .= '<div class="user-signup-box">';
                $fav_html .= '<span class="error_user_exists">User Already Exists!</span>';
                $fav_html .= '<span class="success_signup">Signup Success. Redirecting....</span>';
                $fav_html .= '<input type="text" placeholder="Your Username" id="itmbk-signup-username" required>';
                $fav_html .= '<input type="email" placeholder="Your Email" id="itmbk-signup-email" required>';
                $fav_html .= '<input type="text" placeholder="Your Password" id="itmbk-signup-password" required>';
                $fav_html .= '<input type="button" class="button button4" value="Signup" id="itmbk-signup">';
                $fav_html .= '<p class="already-member">';
                $fav_html .= 'Already a member?';
                $fav_html .= '<a id="itmbk-click-to-login" href="javascript:void(0);" title="Login">Login</a> here.';
                $fav_html .= '</p>';
                $fav_html .= '</div>';
            }
            $fav_html .= '</div>';
            $fav_html .= '</div>';
            $fav_html .= '</div>';
                
            $content .= $fav_html;
        }
    }
    echo $content;
}

//Filter to add single template for collection post type
add_filter( 'the_content', 'collection_single', 10, 1 );
function collection_single( $content ) {
    global $post;
    // Check for single template by post type
    if( strpos($post->post_type, 'cpt_') !== false ) {
        $serialized_bookmarked_posts = get_post_meta( $post->ID, 'bookmarked_post_ids', true );
        $tbl = '';
        
        $tbl .= '<table>';
        $tbl .= '<thead>';
        $tbl .= '<tr>';
        $tbl .= '<th>Post ID</th>';
        $tbl .= '<th>Post Title</th>';
        $tbl .= '<th>Actions</th>';
        $tbl .= '<tr>';
        $tbl .= '</thead>';
        $tbl .= '<tbody class="bookmarked-posts">';
        if( !empty( $serialized_bookmarked_posts ) ) {
            $bookmarked_posts = array_unique( unserialize( $serialized_bookmarked_posts ) );
            foreach( $bookmarked_posts as $bookmarked_post_id ) {
                $bookmarked_post = get_post( $bookmarked_post_id );
                $tbl .= '<tr>';
                $tbl .= '<td>'.$bookmarked_post->ID.'</td>';
                $tbl .= '<td><a href="'.get_permalink( $bookmarked_post->ID ).'">'.$bookmarked_post->post_title.'</a></td>';
                $tbl .= '<td><input id="remove-from-mypost" type="button" value="Remove" data-postid="'.$bookmarked_post->ID.'" data-removefrom="'.$post->ID.'"></td>';
                $tbl .= '<tr>';
            }
        } else {
            $tbl .= '<tr>';
            $tbl .= '<td colspan="3">No Bookmarked Posts</td>';
            $tbl .= '<tr>';
        }
        $tbl .= '</tbody>';
        $tbl .= '</table>';
        
        $content .= $tbl;
        return $content;
    }
}