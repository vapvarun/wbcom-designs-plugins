<?php
if( !is_user_logged_in() ) {
	include 'user-login.php';
} else {
	$users = get_users( array( 'meta_key' => 'bookmarked_posts' ) );
	?>
	<div class="first_header">
		<span class="bookmark_title">List of users who bookmarked any post</span>
	</div>

	<div class="main_div">
		<div class="middle_div">
			<div class="bookmarked_posts">

				<?php if( !empty( $users ) ) {?>
					<div class="three_book">
						<?php echo count( $users );?> users bookmarked any post.
					</div>
					
					<?php foreach( $users as $user ) {?>
						<?php
						$avatar = get_avatar_url( $user->data->ID );
						//echo '<pre>'; print_r( $avatar ); die("here");
						?>
						<div class="partition">
							<div class="img_partition">
								<img src="<?php echo $avatar;?>">
							</div>
							<div class="content_partition">
								<p class="image_blog">
									<?php echo $user->data->user_login;?>
								</p>
								<p class="image_http">
									<?php the_author_meta( 'description', $user->data->ID );?>
								</p>
							</div>
						</div>
					<?php }?>
				<?php } else {?>
					<p>No Bookmarked Items</p>
				<?php }?>
			</div>
		</div>
	</div>
	<?php
}