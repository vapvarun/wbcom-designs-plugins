<?php
if( !is_user_logged_in() ) {
	include 'user-login.php';
} else {
	$userid = get_current_user_id();
	$userdata = get_userdata( $userid );
	//echo '<pre>'; print_r( $userdata ); die("here");

	$user_meta = get_user_meta( $userid, 'bookmarked_posts', true );
	$bookmarked_posts = unserialize( $user_meta );
	?>
	<div class="first_header">
		<span class="bookmark_title"><?php echo $userdata->data->user_login;?>: Bookmarks</span>
	</div>

	<div class="main_div">
		<div class="left_div">
			<p>sample leftbar</p>
		</div>

		<div class="middle_div">
			<div class="bookmarked_posts">

				<?php if( !empty( $bookmarked_posts ) ) {?>
					<div class="three_book">
						<?php echo count( $bookmarked_posts );?> Bookmarks in collection
					</div>
					
					<?php foreach( $bookmarked_posts as $bookmarked_post_id ) {?>
						<?php
						$bookmarked_post = get_post( $bookmarked_post_id );
						$thumbnail_url = wp_get_attachment_thumb_url( get_post_thumbnail_id( $bookmarked_post_id ) );
						if( empty( $thumbnail_url ) ) {
							$thumbnail_url = ITEMS_BOOKMARK_PLUGIN_URL.'assets/images/no_image.png';
						}
						?>
						<div class="partition">
							<div class="img_partition">
								<img src="<?php echo $thumbnail_url;?>">
							</div>
							<div class="content_partition">
								<p class="image_blog">
									<?php echo $bookmarked_post->post_title;?>
								</p>
								<p class="image_http">
									<a href="<?php echo get_permalink( $bookmarked_post_id );?>">
										<?php echo get_permalink( $bookmarked_post_id );?>
									</a>
								</p>
							</div>
							<div class="remove">
								<span class="remove_text">
									<a id="remove-from-bookmark-<?php echo $bookmarked_post_id;?>" class="remove-from-bookmark" href="javascript:void(0);" data-postid="<?php echo $bookmarked_post_id;?>">
										Remove
									</a>
								</span>
							</div>
						</div>
					<?php }?>
				<?php } else {?>
					<p>No Bookmarked Items</p>
				<?php }?>
			</div>
		</div>


		<div class="right_div">
			<span>sample content</span>
		</div>


	</div>
	<?php
}