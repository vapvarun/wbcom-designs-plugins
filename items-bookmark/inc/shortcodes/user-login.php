<div class="full_width">
	<p>Please login before you bookmark this post.</p>
	<div class="user-login-box">
		<span class="error_failed_login">Login Failed. Please Try Again.</span>
		<span class="success_login">Login Success. Redirecting....</span>
		<input type="text" placeholder="Your Username" id="itmbk-login-username" required>
		<input type="password" placeholder="Your Password" id="itmbk-login-password" required>
		<input type="button" class="button button4" value="Login" id="itmbk-login">
		<p class="not-member">
			Not a member?
			<a id="itmbk-click-to-signup" href="javascript:void(0);" title="Register for this site">Register</a> here.
		</p>
	</div>

	<div class="user-signup-box">
		<span class="error_user_exists">User Already Exists!</span>
		<span class="success_signup">Signup Success. Redirecting....</span>
		<input type="text" placeholder="Your Username" id="itmbk-signup-username" required>
		<input type="email" placeholder="Your Email" id="itmbk-signup-email" required>
		<input type="text" placeholder="Your Password" id="itmbk-signup-password" required>
		<input type="button" class="button button4" value="Signup" id="itmbk-signup">
		<p class="already-member">
			Already a member?
			<a id="itmbk-click-to-login" href="javascript:void(0);" title="Login">Login</a> here.
		</p>
	</div>
</div>