<?php
/*
  Plugin Name: Items Bookmark
  Plugin URI: https://wbcomdesigns.com/contact/
  Description: This plugin allows you to bookmark / favourite any post.
  Version: 1.0.0
  Author: Wbcom Designs
  Author URI: http://wbcomdesigns.com
  Text Domain: collections
*/

//Defining constants
$cons = array(
	'ITEMS_BOOKMARK_PLUGIN_PATH'	=>	plugin_dir_path(__FILE__),
	'ITEMS_BOOKMARK_PLUGIN_URL'		=>	plugin_dir_url(__FILE__),
);
foreach( $cons as $con => $value ) {
	define( $con, $value);
}

//Include needed files
$include_files = array(
	'inc/itmbk-scripts.php',
	'inc/itmbk-functions.php',
	'inc/itmbk-cpt.php',
	'inc/itmbk-ajax.php',
	'inc/itmbk-shortcodes.php',
	//'inc/itmbk-template-filters.php',
	'admin/itmbk-admin.php',
);
foreach( $include_files  as $include_file ) {
	include $include_file;
}

//Settings link for this plugin
add_filter( 'plugin_action_links_'.plugin_basename(__FILE__), 'itmbk_admin_page_link' );
function itmbk_admin_page_link( $links ) {
	$page_link = array('<a href="'.admin_url('admin.php?page=itmbk-options').'">Settings</a>');
	return array_merge( $links, $page_link );
}