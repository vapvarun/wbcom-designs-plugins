jQuery(document).ready(function(){

	//New Collection
	jQuery(document).on('click', '#new-collection-btn', function(){
		jQuery( '#new-collection-name' ).show();
		jQuery( '#add-to-new-collection-btn' ).show();
		jQuery( '#new-collection-btn' ).hide();
	});

	//Add Post To New Collection
	jQuery(document).on('click', '#add-to-new-collection-btn', function(){
		var collection_name = jQuery( '#new-collection-name' ).val();
		if( collection_name == '' ) {
			jQuery( '.error_collection_select' ).html( 'Please Enter A Collection Name!' );
			jQuery( '.error_collection_select' ).fadeIn('slow').delay(10000);
            jQuery( '.error_collection_select' ).fadeOut('slow');
		} else {
			var post_id = jQuery( '#add_bookmark' ).data( 'postid' );
			var collection_name = jQuery( '#new-collection-name' ).val();
			console.log( post_id +"    "+ collection_name );
			jQuery.post(
				ajax_object.ajax_url,
				{
					'action' : 'itmbk_add_post_to_new_collection',
					'post_id' : post_id,
					'collection_name' : collection_name,
					'add_to_cpt' : jQuery(this).data('add_to_cpt'),
				},
				function(response) {
					if( response == 'added-to-new-collection-success' ){
						jQuery( '.add_collection_success' ).fadeIn('slow').delay(10000);
	            		jQuery( '.add_collection_success' ).fadeOut('slow');
					}
				}
			);
		}
	});

	//Add post to collection - from opened modal
	jQuery(document).on('click', '#add-to-collection', function(){
		var post_id = jQuery( '#add_bookmark' ).data( 'postid' );
		var collection_id = jQuery( '#collections-list' ).val();
		if( collection_id == 0 ) {
			jQuery( '.error_collection_select' ).fadeIn('slow').delay(10000);
            jQuery( '.error_collection_select' ).fadeOut('slow');
		} else {
			jQuery.post(
				ajax_object.ajax_url,
				{
					'action' : 'itmbk_add_post_to_collection',
					'post_id' : post_id,
					'collection_id' : collection_id,
				},
				function(response) {
					if( response == 'added-to-collection-success' ){
						jQuery( '.add_collection_success' ).fadeIn('slow').delay(10000);
	            		jQuery( '.add_collection_success' ).fadeOut('slow');
					}

					if( response == 'already-added' ) {
						jQuery( '.already_added' ).fadeIn('slow').delay(10000);
 		           		jQuery( '.already_added' ).fadeOut('slow');
					}
				}
			);
		}
	});

	jQuery(document).on('click', '#itmbk-click-to-signup', function(){
		jQuery( '.user-login-box' ).hide();
		jQuery( '.user-signup-box' ).show();
	});

	jQuery(document).on('click', '#itmbk-click-to-login', function(){
		jQuery( '.user-login-box' ).show();
		jQuery( '.user-signup-box' ).hide();
	});

	jQuery(document).on('click', '#itmbk-login', function(){
		var username = jQuery( '#itmbk-login-username' ).val();
		var password = jQuery( '#itmbk-login-password' ).val();
		var curr_url = window.location.href;
		var splitted_url = curr_url.split("#");
		var url_to_redirect = splitted_url[0];
		jQuery.post(
			ajax_object.ajax_url,
			{
				'action' : 'itmbk_user_login',
				'username' : username,
				'password' : password,
			},
			function(response) {
				if( response == 'login-fail' ) {
					jQuery( '.error_failed_login' ).fadeIn('slow').delay(10000);
					jQuery( '.error_failed_login' ).fadeOut('slow');
				} else if( response == 'login-success' ) {
					jQuery( '.success_login' ).fadeIn('slow').delay(10000);
					window.location.href = url_to_redirect;
				}
			}
		);
	});

	jQuery(document).on('click', '#itmbk-signup', function(){
		var username = jQuery( '#itmbk-signup-username' ).val();
		var password = jQuery( '#itmbk-signup-password' ).val();
		var email = jQuery( '#itmbk-signup-email' ).val();
		var curr_url = window.location.href;
		var splitted_url = curr_url.split("#");
		var url_to_redirect = splitted_url[0];
		jQuery.post(
			ajax_object.ajax_url,
			{
				'action' : 'itmbk_user_signup',
				'username' : username,
				'password' : password,
				'email' : email,
			},
			function(response) {
				if( response == 'user-exists' ) {
					jQuery( '.error_user_exists' ).fadeIn('slow').delay(10000);
					jQuery( '.error_user_exists' ).fadeOut('slow');
				} else if( response == 'signup-success' ) {
					jQuery( '.success_signup' ).fadeIn('slow').delay(10000);
					window.location.href = url_to_redirect;
				}
			}
		);
	});

	//Remove from my post
	jQuery(document).on('click', '#remove-from-mypost', function(){
		var remove_post = jQuery(this).data('postid');
		var remove_from_post = jQuery(this).data('removefrom');
		jQuery(this).val('Removing..');
		jQuery.post(
			ajax_object.ajax_url,
			{
				'action' : 'itmbk_remove_from_mypost',
				'remove_post' : remove_post,
				'remove_from_post' : remove_from_post,
			},
			function(response) {
				var html = '';
				if( response == 'no-bookmarked-posts' ) {
					html += '<tr>';
					html += '<td colspan="3">No Bookmarked Posts</td>';
					html += '</tr>';
				} else {
					var posts = response;
					for( i in posts ) {
						var id = posts[i]['id'];
						var title = posts[i]['title'];
						var permalink = posts[i]['permalink'];
						html += '<tr>';
						html += '<td>'+id+'</td>';
						html += '<td><a href="'+permalink+'">'+title+'</a></td>';
						html += '<td><input id="remove-from-mypost" type="button" value="Remove" data-postid="'+id+'" data-removefrom="'+remove_from_post+'"></td>';
						html += '</tr>';
					}
				}

				jQuery( '.bookmarked-posts' ).html( html );
			},
			"JSON"
		);
	});
});