<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Class to add custom scripts and styles
 */
if( !class_exists( 'WcprqScriptsStyles' ) ) {
	class WcprqScriptsStyles{

		/**
		 * Constructor
		 */
		function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'wcprq_custom_variables' ) );
			$curr_url = $_SERVER['REQUEST_URI'];
			if( strpos($curr_url, 'wc-price-quotes-options') !== false ) {
				add_action( 'admin_init', array( $this, 'wcprq_admin_custom_variables' ) );
			}
		}

		/**
		 * Actions performed for enqueuing scripts and styles for site front
		 */
		function wcprq_custom_variables() {
			wp_enqueue_script('wcprq-js-front',WCPRQ_PLUGIN_URL.'assets/js/wcprq-front.js', array('jquery'));
			wp_localize_script( 'wcprq-js-front', 'wcprq_ajax_object',array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
			wp_enqueue_style('wcprq-front-css', WCPRQ_PLUGIN_URL.'assets/css/wcprq-front.css');
		}
		
		/**
		 * Actions performed for enqueuing scripts and styles for admin page
		 */
		function wcprq_admin_custom_variables() {
			wp_enqueue_script('wcprq-js-admin',WCPRQ_PLUGIN_URL.'admin/assets/js/wcprq-admin.js', array('jquery'));
			wp_enqueue_style('wcprq-css-admin', WCPRQ_PLUGIN_URL.'admin/assets/css/wcprq-admin.css');
			wp_enqueue_script('select2-js-admin',WCPRQ_PLUGIN_URL.'admin/assets/js/select2.js', array('jquery'));
			wp_enqueue_style('select2-css-admin', WCPRQ_PLUGIN_URL.'admin/assets/css/select2.css');
		}
	}
	new WcprqScriptsStyles();
}