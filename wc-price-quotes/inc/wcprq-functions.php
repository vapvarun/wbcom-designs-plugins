<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Class to add custom functions used in this plugin
 */
if( !class_exists( 'WcprqFunctions' ) ) {
	class WcprqFunctions{

		/**
		 * Constructor
		 */
		function __construct() {
			add_filter('woocommerce_is_purchasable', array( $this, 'wcprq_product_is_purchasable' ), 10, 2);
			add_action( 'admin_init', array( $this, 'wcprq_cart_page_trashed' ) );
			add_filter( 'woocommerce_get_price_html', array( $this, 'wcprq_hide_prices' ), 10, 2 );
			add_action( 'woocommerce_after_single_product_summary', array( $this, 'wcprq_product_enquiry_form' ) );
		}
		
		function wcprq_product_is_purchasable( $is_purchasable, $product ) {
			$settings = get_option( 'wcprq_settings', true );
			if( !empty( $settings ) ) {
				$settings = unserialize( $settings );
				if( $settings['quote_mode'] == 'selected' ) {
					$products = $settings['products'];
					if( in_array( $product->id , $products ) ) {
						return false;
					} else {
						return $is_purchasable;
					}
				} else if( $settings['quote_mode'] == 'all' ) {
					return false;
				}
			}
		}
		
		/**
		 * Actions performed for hiding the cart page
		 */
		function wcprq_cart_page_trashed() {
			global $wpdb;
			$tbl = $wpdb->prefix."posts";
			$settings = get_option( 'wcprq_settings' );
			if( !empty( $settings ) ) {
				$settings = unserialize( $settings );
				if( $settings['quote_mode'] == 'selected' ) {
					$wpdb->update( 
						$tbl, 
						array( 'post_status' => 'publish' ), 
						array( 'post_title' => 'Cart' ), 
						array( '%s'	), 
						array( '%s' ) 
					);
				} else if( $settings['quote_mode'] == 'all' ) {
					$wpdb->update( 
						$tbl, 
						array( 'post_status' => 'trash' ), 
						array( 'post_title' => 'Cart' ), 
						array( '%s'	), 
						array( '%s' ) 
					);
				}
			}
		}
		
		/**
		 * Actions performed for hiding the product prices
		 */
		function wcprq_hide_prices( $price, $product ) {
			global $product;
			$p_id = $product->id;
			$settings = get_option( 'wcprq_settings', true );
			if( !empty( $settings ) ) {
				$settings = unserialize( $settings );
				if( $settings['quote_mode'] == 'all' ) {
					$price = '';
					/**
					 * These will remove the add-to-cart button
					 * and quantity box from the single product pages
					 * according to the settings in admin panel.
					 */
					remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
  					remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
				} else if( $settings['quote_mode'] == 'selected' ) {
					$products = $settings['products'];
					if( in_array($p_id, $products) ) {
						$price = '';
						
						/**
						 * These will remove the add-to-cart button
						 * and quantity box from the single product pages
						 * according to the settings in admin panel.
						 */
						remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
  						remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
					}
				}
			}
			return $price;
		}
		
		/**
		 * Product Enquiry Form
		 */
		function wcprq_product_enquiry_form() {
			global $product;
			$show_enquiry_form = false;
			
			$p_id = $product->id;
			$settings = get_option( 'wcprq_settings', true );
			if( !empty( $settings ) ) {
				$settings = unserialize( $settings );
				if( $settings['quote_mode'] == 'all' ) {
					$show_enquiry_form = true;
				} else if( $settings['quote_mode'] == 'selected' ) {
					$products = $settings['products'];
					if( in_array($p_id, $products) ) {
						$show_enquiry_form = true;
					}
				}
			}
			
			if( $show_enquiry_form ){
				$name = $email = '';
				if( is_user_logged_in() ) {
					$uid = get_current_user_id();
					$usr = get_userdata($uid);
					$name = $usr->data->display_name;
					$email = $usr->data->user_email;
				}
				?>
				<div class="wcprq-product-enquiry">
					<p style="font-size: 27px;"><?php echo __( 'Product Enquiry', 'wc-price-quotes' );?></p>
					<p class="wcprq-enquiry-save-msg">
						<?php echo __( 'Enquiry saved for the product: '.$product->post->post_title, 'wc-price-quotes' );?>
					</p>
					<table class="wcprq-enquiry-tbl">
						<tr>
							<th><?php echo __( 'Name', 'wc-price-quotes' );?></th>
							<td>
								<input type="text" placeholder="Your Name" id="wcprq_enquiree_name" value="<?php echo $name;?>">
							</td>
						</tr>
						<tr>
							<th><?php echo __( 'Email', 'wc-price-quotes' );?></th>
							<td>
								<input type="email" placeholder="Your Email" id="wcprq_enquiree_email" value="<?php echo $email;?>">
							</td>
						</tr>
						<tr>
							<th><?php echo __( 'Enquiry Summary', 'wc-price-quotes' );?></th>
							<td>
								<textarea placeholder="Enquiry Summary" id="wcprq_enquiree_summary"></textarea>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="hidden" id="wcprq_pruduct_id" value="<?php echo $product->id;?>">
								<input type="button" value="Send Enquiry" id="wcprq_send_enquiry">
								<span class="wait-txt">Please Wait..</span>
							</td>
						</tr>
					</table>
				</div>
				<?php
			} 
		}
	}
	new WcprqFunctions();
}