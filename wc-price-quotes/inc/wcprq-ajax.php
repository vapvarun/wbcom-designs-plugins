<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * AJAX Request to submit the enquiry for the product
 */
add_action( 'wp_ajax_wcprq_send_enquiry', 'wcprq_send_enquiry' );
add_action( 'wp_ajax_nopriv_wcprq_send_enquiry', 'wcprq_send_enquiry' );
if( !function_exists( 'wcprq_send_enquiry' ) ) {
	function wcprq_send_enquiry() {
		if( isset( $_POST['action'] ) && $_POST['action'] === 'wcprq_send_enquiry' ) {
			$name = $_POST['name'];
			$email = $_POST['email'];
			$summary = $_POST['summary'];
			$product_id = $_POST['product_id'];
			$_product = get_post( $product_id );
			$_product_link = get_permalink( $product_id );
			$author = $_product->post_author;
			$prd_author = get_userdata( $author );
			$to = $prd_author->data->user_email;
			$author_uname = $prd_author->data->display_name;
			$subject = '__( "Review for product", "wc-price-quotes" ): '.$_product->post_title;
			$msg = "__( 'Hello', 'wc-price-quotes' ) $author_uname,";
			$msg .= "<p>__( 'There is a request from', 'wc-price-quotes' ) $name __( 'for the product', 'wc-price-quotes' ) '".$_product->post_title."'</p>";
			$msg .= "<p>__( 'He said', 'wc-price-quotes' ): $summary</p>";
			$msg .= "<p>__( 'You can contact the enquiry person by his email', 'wc-price-quotes' ): $email.</p>";
			$msg .= "<p>__( 'Product Link', 'wc-price-quotes' ): <a href='".$_product_link."'>$_product_link</a></p>";
			$msg .= "<p>__( 'Thanks', 'wc-price-quotes' ),</p>";
			$msg .= "<p>".get_bloginfo('name')."</p>";
			$msg .= "<p>(".home_url().")</p>";

			$headers = array('Content-Type: text/html; charset=UTF-8');

			wp_mail($to, $subject, $msg, $headers );
			echo 'enquiry-saved';
			die;
		}
	}
}