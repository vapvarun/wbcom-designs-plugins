=== Woocommerce Price Quotes ===
Contributors: wbcomdesigns
Donate link: https://wbcomdesigns.com/contact/
Tags: woocommerce, price
Requires at least: 3.0.1
Tested up to: 4.6.1
Stable tag: 4.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
 
This plugin helps in quoting the products that admin wishes to hide its purchasing details. The users will not be able to view the purchase details of the product anymore, rather will have an option to send an enquiry regarding the product.
If any user wishes to buy a product whose price has been quoted, then on the product single page, there'll be an enquiry form that will allow the user to send his/her enquiry for the product. The product author will receive a mail for the enquiry and will contact with the customer accordingly.
 
== Installation ==
 
1. Upload the entire "wc-price-quotes" folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
 
== Frequently Asked Questions ==
 
= How can we send an enquiry for any product? =
 
See the products from the store and go to its single description page. There at the bottom you'll see an enquiry form, submitting which will forward your details to the product author.

= How can I (admin) quote a price for any product? =
 
In the admin section, you'll see a menu page just under the woocommerce products namely, "Price Quotes". On that page, you'll have 2options to quote the prices.
First option is for all the products, enabling which will make all the products on the site as not purchasable.
Second option is for selected products where you'll get to select products from a list, and the prices for those products will be quoted.
 
== Screenshots ==

The screenshots are present in the root of the plugin folder.
1. screenshot-1 - is the screen that shows the admin settings page.
2. screenshot-2 - is the screen that shows the store page.
3. screenshot-3 - is the screen that shows the enquiry form on the single product page.