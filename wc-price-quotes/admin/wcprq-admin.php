<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Add admin page for price quote settings
 */
if( !class_exists( 'WcprqAdminPage' ) ) {
	class WcprqAdminPage{

		//constructor
		function __construct() {
			add_action( 'admin_menu', array( $this, 'wcprq_add_menu_page' ) );
		}

		/**
		 * Actions performed on loading admin_menu
		 */
		function wcprq_add_menu_page() {
			$icon_url = WCPRQ_PLUGIN_URL.'admin/assets/images/price-quote.png';
			add_menu_page( __( 'WC Price Quote Settings', 'wc-price-quotes' ), __( 'Price Quotes', 'wc-price-quotes' ), 'manage_options', 'wc-price-quotes-options', array( $this, 'wcprq_admin_options_page' ), $icon_url, 56);
		}

		function wcprq_admin_options_page() {
			include 'wcprq-admin-options-page.php';
		}
	}
	new WcprqAdminPage();
}