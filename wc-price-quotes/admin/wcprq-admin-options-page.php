<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
include 'wcprq-admin-functions.php';
?>
<div class="wcprq-content">
	<div class="wc-woo-logo">
		<img alt="Woo Logo" src="<?php echo WCPRQ_PLUGIN_URL.'admin/assets/images/woocommerce-logo.png';?>">
		<p><?php echo __( 'Price Quote Settings', 'wc-price-quotes' );?></p>
	</div>
	<div class="wcprq-settings">
		<form action="" method="post">
			<div class="wcprq-quote">
				<span><?php echo __( 'Quote Mode', 'wc-price-quotes' );?></span>
				<span>
					<?php 
					if( !empty( $settings ) ) {
						$flag = 1;
						$class = "";
					} else {
						$flag = 0;
						$class = "display";
					}
					?>
					<input <?php if( $flag == 1 && $settings['quote_mode'] == 'all' ) echo 'checked="checked"';?> type="radio" class="wcprq-quote-mode" name="wcprq-quote-mode" value="all-products">All Products
					<input <?php if( $flag == 1 && $settings['quote_mode'] == 'selected' ) echo 'checked="checked"';?> type="radio" class="wcprq-quote-mode" name="wcprq-quote-mode" value="selected-products">Selected Products
				</span>
			</div>
			<div class="wcprq-products <?php echo $class;?>">
				<select multiple name="selected-products-to-quote[]" class="selected-products-to-quote">
					<?php if( !empty( $products ) ) {?>
						<?php foreach( $products as $product ) {?>
							<option <?php if( $flag == 1 && in_array($product->ID, $settings['products']) ) echo 'selected="selected"';?> value="<?php echo $product->ID;?>">
								<?php echo $product->post_title;?>
							</option>
						<?php }?>
					<?php }?>
				</select>
			</div>
			<div class="wcprq-save">
				<input type="submit" name="wcprq-save-settings" class="button button-primary" value="<?php echo __( 'Save', 'wc-price-quotes' );?>">
			</div>
		</form>
	</div>
</div>