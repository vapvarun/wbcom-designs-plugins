jQuery(document).ready(function(){
	jQuery('.selected-products-to-quote').select2();
	/**
	 * Pricing Modes
	 */
	jQuery(document).on('click', '.wcprq-quote-mode', function(){
		var quoteMode = jQuery(this).val();
		if( quoteMode == 'selected-products' )
			jQuery('.wcprq-products').show();
		else
			jQuery('.wcprq-products').hide();
	});
});