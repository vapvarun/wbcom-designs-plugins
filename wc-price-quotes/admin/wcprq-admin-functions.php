<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Save general settings
 */
if( isset( $_POST['wcprq-save-settings'] ) ) {
	$quoteMode = $_POST['wcprq-quote-mode'];
	$settings = array();
	if( $quoteMode == 'all-products' ) {
		$settings = array(
			'quote_mode' => 'all',
			'products' => array(),
		);
	} else {
		$products = $_POST['selected-products-to-quote'];
		$settings = array(
			'quote_mode' => 'selected',
			'products' => $products,
		);
	}
	update_option('wcprq_settings', serialize($settings));
}

/**
 * Retrieve Saved Settings
 */
$settings = get_option( 'wcprq_settings' );
if( !empty( $settings ) ) {
	$settings = unserialize($settings);
} else {
	$settings = array();
}

$args = array(
	'post_type' => 'product',
	'post_status' => 'publish',
	'posts_per_page' => -1
);
$result = new WP_Query( $args );
$products = $result->posts;
//echo '<pre>'; print_r( $settings ); die("here");