<?php
/**
 * Plugin Name: Woocommerce Price Quotes
 * Plugin URI: https://wbcomdesigns.com/contact/
 * Description: This plugin allows the admin to hide the prices of the desired or all the products and allow customers to go for enquiry if they wish to have any product.
 * Version: 1.0.0
 * Author: Wbcom Designs
 * Author URI: http://wbcomdesigns.com
 * License: GPLv2+
 * Text Domain: wc-price-quotes
 * Domain Path: /languages
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
add_action( 'init', 'wcprq_load_textdomain' );
function wcprq_load_textdomain() {
	$domain = "wc-price-quotes";
	$locale = apply_filters( 'plugin_locale', get_locale(), $domain );
	load_textdomain( $domain, 'languages/'.$domain.'-' . $locale . '.mo' );
	$var = load_plugin_textdomain( $domain, false, plugin_basename( dirname(__FILE__) ) . '/languages' );
}

/**
 * Constants used in the plugin
 */
define( 'WCPRQ_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'WCPRQ_PLUGIN_URL', plugin_dir_url(__FILE__) );

/**
 * Include needed files on init
 */
add_action( 'init', 'include_files' );
add_action( 'admin_init', 'include_files' );
function include_files() {
	$include_files = array(
		'inc/wcprq-scripts.php',
		'inc/wcprq-functions.php',
		'inc/wcprq-ajax.php',
		'admin/wcprq-admin.php',
	);
	foreach( $include_files  as $include_file ) include $include_file;
}

/**
 * Plugin Activation
 */
register_activation_hook( __FILE__, 'wcprq_plugin_activation' );
function wcprq_plugin_activation() {
	//Check if "Woocommerce" plugin is active or not
	if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		//Woocommerce Plugin is inactive, hence deactivate this plugin
		deactivate_plugins( plugin_basename( __FILE__ ) );
		wp_die( __( 'The <b>WC Price Quotes</b> plugin requires <b>Woocommerce</b> plugin to be installed and active. Return to <a href="'.admin_url('plugins.php').'">Plugins</a>', 'wc-price-quotes' ) );
	}
}

/**
 * Plugin Deactivation
 */
register_deactivation_hook( __FILE__, 'wcprq_plugin_deactivation' );
function wcprq_plugin_deactivation() {
	global $wpdb;
	$tbl = $wpdb->prefix."posts";
	$wpdb->update( 
		$tbl, 
		array( 'post_status' => 'publish' ), 
		array( 'post_title' => 'Cart' ), 
		array( '%s'	), 
		array( '%s' ) 
	);
}

/**
 * Settings link for this plugin
 */
add_filter( 'plugin_action_links_'.plugin_basename(__FILE__), 'wcprq_admin_page_link' );
function wcprq_admin_page_link( $links ) {
	$page_link = array('<a href="'.admin_url('admin.php?page=wc-price-quotes-options').'">Options</a>');
	return array_merge( $links, $page_link );
}