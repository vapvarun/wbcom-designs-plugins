jQuery(document).ready(function(){
	/**
	 * Save the enquiry form
	 */
	jQuery(document).on('click', '#wcprq_send_enquiry', function(){
		var name = jQuery('#wcprq_enquiree_name').val();
		var email = jQuery('#wcprq_enquiree_email').val();
		var summary = jQuery('#wcprq_enquiree_summary').val();
		var product_id = jQuery('#wcprq_pruduct_id').val();
		jQuery('.wait-txt').show();
		jQuery( this ).attr('disabled',true);
		jQuery.post(
			wcprq_ajax_object.ajax_url,
			{
				'action' : 'wcprq_send_enquiry',
				'name' : name,
				'email' : email,
				'summary' : summary,
				'product_id' : product_id,
			},
			function( response ) {
				jQuery( '#wcprq_send_enquiry' ).attr('disabled',false);
				jQuery('.wait-txt').hide();
				jQuery('.wcprq-enquiry-save-msg').show();
			}
		);
	});
});