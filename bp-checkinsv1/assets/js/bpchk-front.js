jQuery(document).ready(function(){
	//Add Checkin icon under activity update textbox
	var checkin_html = '';
	checkin_html += '<a href="javascript:void(0);" id="position-me" title="Checkin here!">';
	checkin_html += '<span><i class="fa fa-map-marker bpchk-location-marker"></i></span>';
	checkin_html += '</a>';
	checkin_html += '<p class="wait-text"><i>Please Wait...</i></p>';
	checkin_html += '<div class="locations">';
	checkin_html += '<ul id="locations-list">';
	checkin_html += '</ul>';
	checkin_html += '</div>';
	jQuery("#whats-new-textarea").append( checkin_html );

	jQuery(document).on('focusin', '#whats-new-textarea', function(){
		jQuery('#position-me').show();
	});

	//Click icon to getnearby places
    jQuery(document).on("click", "#position-me", function(){
        navigator.geolocation.getCurrentPosition(function success(pos) {
            var crd = pos.coords;
            jQuery( ".wait-text" ).show();
            jQuery.post(
                ajaxurl,
                {
                    'action' : 'bpchk_get_locations',
                    'latitude' : crd.latitude,
                    'longitude' : crd.longitude,
                },
                function( response ) {
                    jQuery( ".wait-text" ).hide();
                    jQuery( ".locations" ).show();
                    var li_html = '';
                    
                    for( i in response ){
                        var div_html = '';
                        
                        div_html += '<div>';
                        div_html += '<img height="25px" width="25px" title="'+response[i]['name']+'" src="'+response[i]['icon']+'">';
                        div_html += '<div class="loc-title">';
                        div_html += '<h3>'+response[i]['name']+'</h3>';
                        div_html += '<span>'+response[i]['vicinity']+'</span>';
                        div_html += '</div>';
                        div_html += '</div>';
                        
                        li_html += '<li class="single-location" data-reference="'+response[i]['reference']+'" id="'+response[i]['place_id']+'">';
                        li_html += div_html;
                        li_html += '</li>';
                    }
                    jQuery( "#locations-list" ).html( li_html )
                },
                "JSON"
            );
        });
    });
});