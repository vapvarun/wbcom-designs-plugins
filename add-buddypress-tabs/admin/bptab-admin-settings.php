<?php
$spinner_src = includes_url().'/images/spinner.gif';
$wp_spinner_src = includes_url().'/images/wpspin.gif';
?>
<div class="bptab-bp-logo">
	<img src="<?php echo BPTAB_PLUGIN_URL.'admin/assets/images/buddypress_logo.png';?>" alt="BP Logo">
</div>
<div class="bptab-logo-txt">
	<h1>Add Custom BP Tabs In Member's Profile Or Groups</h1>
</div>

<!-- BP TABS FOR GROUPS -->
<div class="add-bp-tab-groups">
	<p>BP Tabs: <b>Groups</b></p>
	
</div>

<!-- BP TABS FOR GROUPS -->
<div class="add-bp-tab-profiles">
	<p>BP Tabs: <b>Member's Profile</b></p>
	<select id="bp-profile-type">
		<option value="">--Select--</option>
		<option value="new-parent">New Parent</option>
		<option value="old-parent">Already Parent</option>
	</select>
	<div class="new-parent-tab">
		<p>Add new parent tab with child tabs in following structure:</p>
		<p>
			<input type="text" placeholder="Parent tab title" id="new-parent-profile-tab">
			<select id="parent-content">
				<option value="">--Select--</option>
				<option value="shortcode">Shortcode</option>
				<option value="post_page">Page Content</option>
				<option value="text">Simple Text</option>
			</select>
		</p>
		<p>
			<input type="text" placeholder="Child tab title" id="new-chile-profile-tab-1" class="bp-profile-new-child">
			<input type="text" placeholder="Child tab title" id="new-chile-profile-tab-2" class="bp-profile-new-child">
			<input type="text" placeholder="Child tab title" id="new-chile-profile-tab-3" class="bp-profile-new-child">
		</p>
		<p>
			<input type="button" class="button button-primary bp-profile-add-new-tab" value="Add Tabs">
			<img src="<?php echo $spinner_src;?>" alt="Spinner" id="add-profile-new-tabs-spinner" class="ajax-spinner">
		</p>
	</div>
	<div class="old-parent-tab">
		<p>Select the parent tab under which you wish to add a new sub tab:</p>
		<select class="bp-grp-old-tabs">
			<option value="">--Select--</option>
		</select>
	</div>

	<div class="new-parents-list">
		<h4>List Of The New Parent Tabs</h4>
		<?php include 'new-parents-list.php';?>
	</div>
</div>