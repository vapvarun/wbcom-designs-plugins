<?php 
$tabs = get_option( 'bp_member_new_tabs', true );
if( !empty( $tabs ) ) {
	$tabs = unserialize( $tabs );
}
?>
<table class="wp-list-table widefat fixed">
	<thead>
		<tr>
			<th>
				<a href="javascript:void(0);">
					<span>Parent Tab</span>
				</a>
			</th>
			<th>
				<a href="javascript:void(0);">
					<span>Action</span>
				</a>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php if( !isset( $tabs ) ) {?>
			<?php foreach( $tabs as $key => $tab ) {?>
				<tr>
					<td><?php echo $key;?></td>
					<td>
						<a class="del-parent" href="javascript:void(0);" data-tab="<?php echo $key;?>" title="Delete <?php echo $key;?>">
							<img class="del-parent-icon" src="<?php echo BPTAB_PLUGIN_URL.'admin/assets/images/delete.png';?>">
						</a>
					</td>
				</tr>
			<?php }?>
		<?php } else {?>
			<tr>
				<td colspan="2">
					<p style="margin: 0px;">No New Parent Tabs Created!</p>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>