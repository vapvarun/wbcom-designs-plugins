<?php
//Creating menu page - add bp tabs admin options
add_action('admin_menu', 'bptab_admin_page');
function bptab_admin_page() {
	add_menu_page( 'Add BP Tabs', 'BP Tabs', 'manage_options', 'bptab-options', 'bptab_options_page', 'dashicons-plus', 3 );
}
function bptab_options_page() {
	include 'bptab-admin-settings.php';
}