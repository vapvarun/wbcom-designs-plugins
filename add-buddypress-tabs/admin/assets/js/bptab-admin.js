jQuery(document).ready(function(){
	jQuery(document).on('change', '#bp-profile-type', function(){
		var profile_type = jQuery( this ).val();
		if( profile_type == 'new-parent' ) {
			jQuery( '.new-parent-tab' ).show();
			jQuery( '.old-parent-tab' ).hide();
		}
		if( profile_type == 'old-parent' ) {
			jQuery( '.new-parent-tab' ).hide();
			jQuery( '.old-parent-tab' ).show();
		}
		if( profile_type == '' ) {
			jQuery( '.new-parent-tab' ).hide();
			jQuery( '.old-parent-tab' ).hide();
		}
	});

	jQuery(document).on('click', '.bp-profile-add-new-tab', function(){
		var children = [];
		for( i = 1; i <= 3; i++ ) {
			children.push( jQuery( '#new-chile-profile-tab-'+i ).val() );
		}
		var parent = jQuery( '#new-parent-profile-tab' ).val();
		jQuery( '#add-profile-new-tabs-spinner' ).show();
		jQuery( '.bp-profile-add-new-tab' ).addClass( 'bptab-btn-ajax' );
		jQuery.post(
			ajaxurl,
			{
				'action' : 'bptab_add_new_profile_tabs',
				'parent' : parent,
				'children' : children,
			},
			function( response ) {
				jQuery( '#add-profile-new-tabs-spinner' ).hide();
				jQuery( '.bp-profile-add-new-tab' ).removeClass( 'bptab-btn-ajax' );
			}
		);
	});

	jQuery(document).on('click', '.del-parent', function(){
		var tab = jQuery( this ).data( 'tab' );
		jQuery(this).closest('tr').css( 'background-color', '#F75D59' );
		jQuery.post(
			ajaxurl,
			{
				'action' : 'bptab_del_new_profile_tab',
				'tab' : tab,
			},
			function( response ) {
				console.log( response );
			}
		);
	});
});