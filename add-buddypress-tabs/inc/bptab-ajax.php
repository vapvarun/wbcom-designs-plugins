<?php
//Add new tabs in member profile
add_action( 'wp_ajax_bptab_add_new_profile_tabs', 'bptab_add_new_profile_tabs');
add_action( 'wp_ajax_nopriv_bptab_add_new_profile_tabs', 'bptab_add_new_profile_tabs');
function bptab_add_new_profile_tabs() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'bptab_add_new_profile_tabs' ) {
		global $bp;
		$parent = $_POST['parent'];
		$children = $_POST['children'];
		
		$bp_profile_tabs = array();
		$parent_slug = strtolower( str_replace( ' ', '_', $parent) );
		//Adding parent tab
		$bp_profile_tabs[] = array(
			'type' => 'parent',
			'name' => $parent,
			'slug' => $parent_slug,
			'screen_function' => $parent_slug.'_show_screen',
			'position' => 75,
			'default_subnav_slug' => $parent_slug.'_sub',
			'show_for_displayed_user' => 'true',
		);
		foreach( $children as $child ) {
			if( $child != '' ){
				$child_slug = strtolower( str_replace( ' ', '_', $child) );
				$bp_profile_tabs[] = array(
					'type' => 'child',
					'name' => $child,
					'slug' => $child_slug,
					'parent_url' => '$bp->loggedin_user->domain . $parent_slug."/"',
					'parent_slug' => $parent_slug,
					'screen_function' => $child_slug.'_show_screen',
					'position' => 100,
					'link' => 'site_url()."/members/$name/$parent_slug/$child_slug/"',
				);
			}
		}

		//echo '<pre>'; print_r( $bp_profile_tabs ); die("here");

		//Creating file by the name of parent tab
		$file = 'member_tab_new_'.$parent_slug.'.php';
		$file_path = BPTAB_PLUGIN_PATH.'inc/member-tab-new/member_tab_new_'.$parent_slug.'.php';
		$parent_tab_file = fopen( $file_path, "w" );
		$txt = '<?php';
		$txt .= "\n";
		$txt .= 'add_action( "bp_setup_nav", "bptab_member_profile_'.$parent_slug.'_tab", 301 );';
		$txt .= "\n";
		$txt .= 'function bptab_member_profile_'.$parent_slug.'_tab(){';
			$txt .= "\n";
			$txt .= '	global $bp;';
			$txt .= "\n";
			$txt .= '	$parent_slug = "'.$parent_slug.'";';
			$txt .= "\n";
			$txt .= '	$name = bp_get_displayed_user_username();';
			$txt .= "\n";
			foreach( $bp_profile_tabs as $bp_profile_tab ) {
				if( $bp_profile_tab['type'] == 'parent' ) {
					$txt .= '	bp_core_new_nav_item( array(';
					$txt .= "\n";
					$txt .= '		"name" => "'.$bp_profile_tab['name'].'",';
					$txt .= "\n";
					$txt .= '		"slug" => "'.$bp_profile_tab['slug'].'",';
					$txt .= "\n";
					$txt .= '		"screen_function" => "'.$bp_profile_tab['screen_function'].'",';
					$txt .= "\n";
					$txt .= '		"position" => '.$bp_profile_tab['position'].',';
					$txt .= "\n";
					$txt .= '		"default_subnav_slug" => "'.$bp_profile_tab['default_subnav_slug'].'",';
					$txt .= "\n";
					$txt .= '		"show_for_displayed_user" => '.$bp_profile_tab['show_for_displayed_user'].',';
					$txt .= "\n";
					$txt .= '	));';
					$txt .= "\n\n";
				} else {
					$txt .= '	$child_slug = "'.$bp_profile_tab['slug'].'";';
					$txt .= "\n";
					$txt .= '	bp_core_new_nav_item( array(';
					$txt .= "\n";
					$txt .= '		"name" => "'.$bp_profile_tab['name'].'",';
					$txt .= "\n";
					$txt .= '		"slug" => "'.$bp_profile_tab['slug'].'",';
					$txt .= "\n";
					$txt .= '		"parent_url" => '.$bp_profile_tab['parent_url'].',';
					$txt .= "\n";
					$txt .= '		"parent_slug" => "'.$bp_profile_tab['parent_slug'].'",';
					$txt .= "\n";
					$txt .= '		"screen_function" => "'.$bp_profile_tab['screen_function'].'",';
					$txt .= "\n";
					$txt .= '		"position" => '.$bp_profile_tab['position'].',';
					$txt .= "\n";
					$txt .= '		"link" => '.$bp_profile_tab['link'].',';
					$txt .= "\n";
					$txt .= '	));';
					$txt .= "\n";
				}
			}
		$txt .= "\n";
		$txt .= '}';
		$txt .= "\n\n";
		foreach( $bp_profile_tabs as $bp_profile_tab ) {
			if( $bp_profile_tab['type'] == 'parent' ) {
				$txt .= 'function '.$bp_profile_tab['screen_function'].'() {';
				$txt .= "\n";
				$txt .= '	add_action( "bp_template_title", "bptab_'.$parent_slug.'_tab_function_to_show_title" );';
				$txt .= "\n";
				$txt .= '	add_action( "bp_template_content", "bptab_'.$parent_slug.'_tab_function_to_show_content" );';
				$txt .= "\n";
				$txt .= '	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );';
				$txt .= "\n";
				$txt .= '}';
				$txt .= "\n";
				$txt .= 'function bptab_'.$parent_slug.'_tab_function_to_show_title() {';
				$txt .= "\n";
				$txt .= '	echo "Sample Title";';
				$txt .= "\n";
				$txt .= '}';
				$txt .= "\n";
				$txt .= 'function bptab_'.$parent_slug.'_tab_function_to_show_content() {';
				$txt .= "\n";
				$txt .= '	echo "Sample Content";';
				$txt .= "\n";
				$txt .= '}';
				$txt .= "\n\n";
			} else {
				$txt .= 'function '.$bp_profile_tab['screen_function'].'() {';
				$txt .= "\n";
				$txt .= '	add_action( "bp_template_title", "bptab_'.$bp_profile_tab['slug'].'_tab_function_to_show_title" );';
				$txt .= "\n";
				$txt .= '	add_action( "bp_template_content", "bptab_'.$bp_profile_tab['slug'].'_tab_function_to_show_content" );';
				$txt .= "\n";
				$txt .= '	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );';
				$txt .= "\n";
				$txt .= '}';
				$txt .= "\n";
				$txt .= 'function bptab_'.$bp_profile_tab['slug'].'_tab_function_to_show_title() {';
				$txt .= "\n";
				$txt .= '	echo "Sample Title";';
				$txt .= "\n";
				$txt .= '}';
				$txt .= "\n";
				$txt .= 'function bptab_'.$bp_profile_tab['slug'].'_tab_function_to_show_content() {';
				$txt .= "\n";
				$txt .= '	echo "Sample Content";';
				$txt .= "\n";
				$txt .= '}';
				$txt .= "\n\n";
			}
		}

		$txt = rtrim( $txt );
		fwrite( $parent_tab_file, $txt );
		fclose( $parent_tab_file );

		$new_tabs = get_option( 'bp_member_new_tabs', true );
		if( empty( $new_tabs ) ){
			$new_tabs[$parent] = $file;
			update_option( 'bp_member_new_tabs', serialize( $new_tabs ) );
		} else {
			$new_tabs = unserialize( $new_tabs );
			$new_tabs[$parent] = $file;
			$new_tabs = array_unique( $new_tabs );
			update_option( 'bp_member_new_tabs', serialize( $new_tabs ) );
		}
		echo 'new-member-tab-added';
		die;
	}
}

//Del newly added tab on profile
add_action( 'wp_ajax_bptab_del_new_profile_tab', 'bptab_del_new_profile_tab');
add_action( 'wp_ajax_nopriv_bptab_del_new_profile_tab', 'bptab_del_new_profile_tab');
function bptab_del_new_profile_tab() {
	if( isset( $_POST['action'] ) && $_POST['action'] === 'bptab_del_new_profile_tab' ) {
		$tab = $_POST['tab'];
		$tabs = unserialize( get_option( 'bp_member_new_tabs', true ) );
		$tab_slug = strtolower( str_replace( ' ', '_', $tab) );
		$file_to_delete = BPTAB_PLUGIN_PATH.'inc/member-tab-new/member_tab_new_'.$tab_slug.'.php';
		
		//unlink( $file_to_delete );
		//unset( $tabs[$tab] );
		
		echo 'tab-deleted'; die;
	}
}

// add_action( 'init', function() {
// 	$users = get_users();
// 	echo '<pre>'; print_r($users); die;
// } );