<?php
add_action( "bp_setup_nav", "bptab_member_profile_sample_tab", 301 );
function bptab_member_profile_sample_tab(){
	global $bp;
	$parent_slug = "sample";
	$name = bp_get_displayed_user_username();
	bp_core_new_nav_item( array(
		"name" => "Sample",
		"slug" => "sample",
		"screen_function" => "sample_show_screen",
		"position" => 75,
		"default_subnav_slug" => "sample_sub",
		"show_for_displayed_user" => true,
	));

	$child_slug = "sample_child1";
	bp_core_new_nav_item( array(
		"name" => "Sample Child1",
		"slug" => "sample_child1",
		"parent_url" => $bp->loggedin_user->domain . $parent_slug."/",
		"parent_slug" => "sample",
		"screen_function" => "sample_child1_show_screen",
		"position" => 100,
		"link" => site_url()."/members/$name/$parent_slug/$child_slug/",
	));
	$child_slug = "sample_child2";
	bp_core_new_nav_item( array(
		"name" => "Sample Child2",
		"slug" => "sample_child2",
		"parent_url" => $bp->loggedin_user->domain . $parent_slug."/",
		"parent_slug" => "sample",
		"screen_function" => "sample_child2_show_screen",
		"position" => 100,
		"link" => site_url()."/members/$name/$parent_slug/$child_slug/",
	));

}

function sample_show_screen() {
	add_action( "bp_template_title", "bptab_sample_tab_function_to_show_title" );
	add_action( "bp_template_content", "bptab_sample_tab_function_to_show_content" );
	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );
}
function bptab_sample_tab_function_to_show_title() {
	echo "Sample Title";
}
function bptab_sample_tab_function_to_show_content() {
	echo "Sample Content";
}

function sample_child1_show_screen() {
	add_action( "bp_template_title", "bptab_sample_child1_tab_function_to_show_title" );
	add_action( "bp_template_content", "bptab_sample_child1_tab_function_to_show_content" );
	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );
}
function bptab_sample_child1_tab_function_to_show_title() {
	echo "Sample Title";
}
function bptab_sample_child1_tab_function_to_show_content() {
	echo "Sample Content";
}

function sample_child2_show_screen() {
	add_action( "bp_template_title", "bptab_sample_child2_tab_function_to_show_title" );
	add_action( "bp_template_content", "bptab_sample_child2_tab_function_to_show_content" );
	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );
}
function bptab_sample_child2_tab_function_to_show_title() {
	echo "Sample Title";
}
function bptab_sample_child2_tab_function_to_show_content() {
	echo "Sample Content";
}