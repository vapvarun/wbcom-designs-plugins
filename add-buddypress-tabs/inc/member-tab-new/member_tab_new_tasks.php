<?php
add_action( "bp_setup_nav", "bptab_member_profile_tasks_tab", 301 );
function bptab_member_profile_tasks_tab(){
	global $bp;
	$parent_slug = "tasks";
	$name = bp_get_displayed_user_username();
	bp_core_new_nav_item( array(
		"name" => "Tasks",
		"slug" => "tasks",
		"screen_function" => "tasks_show_screen",
		"position" => 75,
		"default_subnav_slug" => "tasks_sub",
		"show_for_displayed_user" => true,
	));

	$child_slug = "task_list";
	bp_core_new_nav_item( array(
		"name" => "Task List",
		"slug" => "task_list",
		"parent_url" => $bp->loggedin_user->domain . $parent_slug."/",
		"parent_slug" => "tasks",
		"screen_function" => "task_list_show_screen",
		"position" => 100,
		"link" => site_url()."/members/$name/$parent_slug/$child_slug/",
	));
	$child_slug = "tasks_to_do";
	bp_core_new_nav_item( array(
		"name" => "Tasks To Do",
		"slug" => "tasks_to_do",
		"parent_url" => $bp->loggedin_user->domain . $parent_slug."/",
		"parent_slug" => "tasks",
		"screen_function" => "tasks_to_do_show_screen",
		"position" => 100,
		"link" => site_url()."/members/$name/$parent_slug/$child_slug/",
	));

}

function tasks_show_screen() {
	add_action( "bp_template_title", "bptab_tasks_tab_function_to_show_title" );
	add_action( "bp_template_content", "bptab_tasks_tab_function_to_show_content" );
	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );
}
function bptab_tasks_tab_function_to_show_title() {
	echo "Sample Title";
}
function bptab_tasks_tab_function_to_show_content() {
	echo "Sample Content";
}

function task_list_show_screen() {
	add_action( "bp_template_title", "bptab_task_list_tab_function_to_show_title" );
	add_action( "bp_template_content", "bptab_task_list_tab_function_to_show_content" );
	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );
}
function bptab_task_list_tab_function_to_show_title() {
	echo "Sample Title";
}
function bptab_task_list_tab_function_to_show_content() {
	echo "Sample Content";
}

function tasks_to_do_show_screen() {
	add_action( "bp_template_title", "bptab_tasks_to_do_tab_function_to_show_title" );
	add_action( "bp_template_content", "bptab_tasks_to_do_tab_function_to_show_content" );
	bp_core_load_template( apply_filters( "bp_core_template_plugin", "members/single/plugins" ) );
}
function bptab_tasks_to_do_tab_function_to_show_title() {
	echo "Sample Title";
}
function bptab_tasks_to_do_tab_function_to_show_content() {
	echo "Sample Content";
}