<?php
//Register Custom Variables & Scripts for admin options
add_action('admin_init', 'bptab_custom_variables');
function bptab_custom_variables() {
	if( isset( $_GET['page'] ) && ( $_GET['page'] == 'bptab-options' ) ) {
		wp_enqueue_script( 'bptab-js-admin', BPTAB_PLUGIN_URL.'admin/assets/js/bptab-admin.js', array( 'jquery' ) );
	  	wp_enqueue_style( 'bptab-css-admin', BPTAB_PLUGIN_URL.'admin/assets/css/bptab-admin.css' );

	  	//Select2 JS & CSS
	  	wp_enqueue_script( 'bptab-select2-js', BPTAB_PLUGIN_URL.'admin/assets/js/select2.js', array( 'jquery' ) );
	  	wp_enqueue_style( 'bptab-select2-css', BPTAB_PLUGIN_URL.'admin/assets/css/select2.css' );
	}
}