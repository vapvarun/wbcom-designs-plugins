<?php
/*
  Plugin Name: Add BuddyPress Tabs
  Plugin URI: https://wbcomdesigns.com/contact/
  Description: This plugin allows you to add custom buddypress tabs in groups or profile.
  Version: 1.0.0
  Author: Wbcom Designs
  Author URI: http://wbcomdesigns.com
  Text Domain: bp-tabs
*/

//Defining constants
$cons = array(
	'BPTAB_PLUGIN_PATH' => plugin_dir_path(__FILE__),
	'BPTAB_PLUGIN_URL' => plugin_dir_url(__FILE__),
);
foreach( $cons as $con => $value ) {
	define( $con, $value);
}

//Include needed files
$include_files = array(
	'admin/bptab-admin.php',
	'inc/bptab-scripts.php',
	'inc/bptab-ajax.php',
);
//Include new created files while adding new tab in profile
$new_tab_files = get_option( 'bp_member_new_tabs', true );
if( !empty( $new_tab_files ) ) {
	$new_tab_files = unserialize( $new_tab_files );
	foreach( $new_tab_files as $new_tab_file ) {
		$include_files[] = 'inc/member-tab-new/'.$new_tab_file;
	}
}
foreach( $include_files  as $include_file ) {
	include $include_file;
}

//BP Checkins Plugin Activation
register_activation_hook( __FILE__, 'BPTAB_plugin_activation' );
function BPTAB_plugin_activation() {
	//Check if "Buddypress" plugin is active or not
	if (!in_array('buddypress/bp-loader.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		//Buddypress Plugin is inactive, hence deactivate this plugin
		deactivate_plugins( plugin_basename( __FILE__ ) );
		wp_die( 'The <b>BP Checkins</b> plugin requires <b>Buddypress</b> plugin to be installed and active. Return to <a href="'.admin_url('plugins.php').'">Plugins</a>' );
	}
}

//Settings link for this plugin
add_filter( 'plugin_action_links_'.plugin_basename(__FILE__), 'bptab_admin_page_link' );
function bptab_admin_page_link( $links ) {
	$page_link = array('<a href="'.admin_url('admin.php?page=bptab-options').'">Options</a>');
	return array_merge( $links, $page_link );
}