<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Profile Integration Tab - Checkins
if( !class_exists( 'BpchkProfileIntegrationCheckins' ) ) {
	class BpchkProfileIntegrationCheckins{

		//Constructor
		function __construct() {
			add_action( 'bp_setup_nav', array( $this, 'bpchk_member_profile_checkins_tab' ), 301 );
		}

		//Actions performed to create checkins profile integration tab
		function bpchk_member_profile_checkins_tab() {
			if( is_user_logged_in() ) {
				global $bp;
				$name = bp_get_displayed_user_username();
				$tab_args = array(
					'name' => 'Checkins',
					'slug' => 'checkins',
					'screen_function' => array( $this, 'my_profile_checkins_tab_function_to_show_screen' ),
					'position' => 75,
					'default_subnav_slug' => 'checkins_sub',
					'show_for_displayed_user' => true,
				);
				bp_core_new_nav_item( $tab_args );

				$parent_slug = 'checkins';

				//Add subnav checkins - already made
				bp_core_new_subnav_item(
					array(
						'name' => 'Checked in',
						'slug' => 'checked-in',
						'parent_url' => $bp->loggedin_user->domain . $parent_slug.'/',
						'parent_slug' => $parent_slug,
						'screen_function' => array( $this, 'bpchk_checkedin_show_screen' ),
						'position' => 100,
						'link' => site_url()."/members/$name/$parent_slug/checked-in/",
					)
				);

				$loggedin_user_id = bp_loggedin_user_id();
				$displayed_user_id = bp_displayed_user_id();
				if( $loggedin_user_id === $displayed_user_id ) {
					//Add subnav checkins - to check in
					bp_core_new_subnav_item(
						array(
							'name' => ' To checkin',
							'slug' => 'to-checkin',
							'parent_url' => $bp->loggedin_user->domain . $parent_slug.'/',
							'parent_slug' => $parent_slug,
							'screen_function' => array( $this, 'bpchk_to_checkin_show_screen' ),
							'position' => 100,
							'link' => site_url()."/members/$name/$parent_slug/to-checkin/",
						)
					);
				}
			}
		}

		//Actions performed to show screen of checkins tab
		function my_profile_checkins_tab_function_to_show_screen() {
			$loggedin_user_id = bp_loggedin_user_id();
			$displayed_user_id = bp_displayed_user_id();
			$name = bp_get_displayed_user_username();
			if( $loggedin_user_id === $displayed_user_id ) {
				$myCheckinsURL = home_url().'/members/'.$name.'/checkins/to-checkin';
			} else {
				$myCheckinsURL = home_url().'/members/'.$name.'/checkins/checked-in';
			}
			header( 'Location: '.$myCheckinsURL );
		}

		//Actions performed to show screen of places that are checked in
		function bpchk_checkedin_show_screen() {
			add_action( 'bp_template_title', array( $this, 'bpchk_checkedin_tab_function_to_show_title' ) );
			add_action( 'bp_template_content', array( $this, 'bpchk_checkedin_tab_function_to_show_content' ) );
			bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
		}

		//Actions performed to show screen title of checkedin tab
		function bpchk_checkedin_tab_function_to_show_title() {
			echo 'The following are the places you\'ve checked in';
		}

		//Actions performed to show screen content of checkedin tab
		function bpchk_checkedin_tab_function_to_show_content() {
			include 'checkins/checkedin-places.php';
		}

		//Actions performed to show screen of favourite outfits tab
		function bpchk_to_checkin_show_screen() {
			add_action( 'bp_template_title', array( $this, 'bpchk_to_checkin_tab_function_to_show_title' ) );
			add_action( 'bp_template_content', array( $this, 'bpchk_to_checkin_tab_function_to_show_content' ) );
			bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
		}

		//Actions performed to show screen title of to-checkin tab
		function bpchk_to_checkin_tab_function_to_show_title() {
			echo 'Add places to make future checkins';
		}

		//Actions performed to show screen content of to-checkin tab
		function bpchk_to_checkin_tab_function_to_show_content() {
			include 'checkins/checkins-to-make.php';
		}
	}
	new BpchkProfileIntegrationCheckins();
}