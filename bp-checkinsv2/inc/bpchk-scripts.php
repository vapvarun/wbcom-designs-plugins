<?php 
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

//Class to add custom scripts and styles
if( !class_exists( 'BpchkScriptsStyles' ) ) {
    class BpchkScriptsStyles{

        //Constructor
        function __construct() {
            $curr_url = $_SERVER['REQUEST_URI'];
            if( strpos($curr_url, 'checkins') !== false ) {
                add_action( 'wp_enqueue_scripts', array( $this, 'bpchk_custom_variables' ) );
            }
        }

        //Actions performed for enqueuing scripts and styles for site front
        function bpchk_custom_variables() {
            $apiKey = 'AIzaSyAX3IXfDPR5Iex5ko7y3WGlxpIyLznphhg';
			wp_enqueue_script('bpchk-google-api-js', 'https://maps.googleapis.com/maps/api/js?libraries=places&key='.$apiKey, array('jquery'));
			wp_enqueue_script('bpchk-js-front',BPCHK_PLUGIN_URL.'assets/js/bpchk-front.js', array('jquery'));
			wp_localize_script( 'bpchk-js-front', 'bpchk_ajax_object',array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
			wp_enqueue_style('bpchk-front-css', BPCHK_PLUGIN_URL.'assets/css/bpchk-front.css');

			//Font Awesome
			wp_enqueue_style('bpchk-fa-css', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
        }
    }
    new BpchkScriptsStyles();
}