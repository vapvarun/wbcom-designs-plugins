<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

//Class to serve AJAX Calls
if( !class_exists( 'BpchkAjax' ) ) {
    class BpchkAjax{

        //Constructor
        function __construct() {
        	//Add Place To Checkin
            add_action( 'wp_ajax_bpchk_add_place_to_checkin', array( $this, 'bpchk_add_place_to_checkin' ) );
            add_action( 'wp_ajax_nopriv_bpchk_add_place_to_checkin', array( $this, 'bpchk_add_place_to_checkin' ) );
            
            //Mark Place As Visited
            add_action( 'wp_ajax_bpchk_mark_place_visited', array( $this, 'bpchk_mark_place_visited' ) );
            add_action( 'wp_ajax_nopriv_bpchk_mark_place_visited', array( $this, 'bpchk_mark_place_visited' ) );

            //Edit a place
            add_action( 'wp_ajax_bpchk_update_place', array( $this, 'bpchk_update_place' ) );
            add_action( 'wp_ajax_nopriv_bpchk_update_place', array( $this, 'bpchk_update_place' ) );

            //Delete a place
            add_action( 'wp_ajax_bpchk_delete_place', array( $this, 'bpchk_delete_place' ) );
            add_action( 'wp_ajax_nopriv_bpchk_delete_place', array( $this, 'bpchk_delete_place' ) );
        }

        //Actions performed to add Place To Checkin
        function bpchk_add_place_to_checkin() {
            if( isset( $_POST['action'] ) && $_POST['action'] === 'bpchk_add_place_to_checkin' ) {
                global $wpdb;
				$tbl = $wpdb->prefix."bp_future_checkins";
				$place = $_POST['place'];
				$all_places = $_POST['all_places'];
				

				$wpdb->insert( 
					$tbl, 
					array( 
						'place' => $place, 
						'visited' => 'no',
						'uid' => get_current_user_id(),
					), 
					array( 
						'%s', 
						'%s',
						'%d',
					)
				);

				//Updating the hidden JSON
				$all_places[] = $place;
				
				$sql = "SELECT `id` FROM `$tbl` WHERE `place` = '$place'";
				$result = $wpdb->get_results( $sql );
				$pid = $result[0]->id;
				$arr = array(
					'msg' => 'place-add-success',
					'pid' => $pid,
					'all_places' => json_encode( $all_places ),
				);
				echo json_encode( $arr );
				die;
            }
        }

        //Actions performed to mark place as visited
        function bpchk_mark_place_visited() {
            if (isset($_POST['action']) && $_POST['action'] === 'bpchk_mark_place_visited') {
                global $wpdb;
				$tbl = $wpdb->prefix."bp_future_checkins";
				$activity_tbl = $wpdb->prefix.'bp_activity';
				$pid = $_POST['placeid'];
				$place = $_POST['place'];

				$wpdb->update( 
					$tbl, 
					array( 'visited' => 'yes' ), 
					array( 'id' => $pid ), 
					array( '%s' ), 
					array( '%d' )
				);

				$placeurl = 'https://www.google.co.in/maps/place/'.str_replace( ', ', '+', $place );

				//Add activity for the place visited
				$uid = get_current_user_id();
				$user_data = get_userdata( $uid );
				$user_url = home_url().'/members/'.$user_data->data->user_login;
				$action = '<a href="'.$user_url.'">'.$user_data->data->user_login.'</a> checkedin at <a href="'.$placeurl.'">'.$place.'</a>';
				$wpdb->insert( 
					$activity_tbl, 
					array( 
						'user_id' => $uid, 
						'component' => 'activity',
						'type' => 'activity_update', 
						'action' => $action,
						'content' => $action, 
						'primary_link' => $user_url,
						'item_id' => $uid, 
						'date_recorded' => gmdate( "Y-m-d H:i:s" ),
						'hide_sitewide' => 0, 
						'is_spam' => 0
					), 
					array( 
						'%d', 
						'%s',
						'%s', 
						'%s',
						'%s', 
						'%s',
						'%d', 
						'%s',
						'%d', 
						'%d', 
					) 
				);
				echo 'place-marked-visited-success';
				die;
            }
        }

        //Actions performed to update a place
        function bpchk_update_place() {
            if (isset($_POST['action']) && $_POST['action'] === 'bpchk_update_place') {
                global $wpdb;
				$tbl = $wpdb->prefix."bp_future_checkins";
				$pid = $_POST['placeid'];
				$new_place = $_POST['place'];
				
				$wpdb->update( 
					$tbl, 
					array( 'place' => $new_place ),
					array( 'id' => $pid ),
					array( '%s'	),
					array( '%d' )
				);
				echo 'place-updated';
				die;
            }
        }

        //Actions performed to delete a place
        function bpchk_delete_place() {
            if (isset($_POST['action']) && $_POST['action'] === 'bpchk_delete_place') {
                global $wpdb;
				$tbl = $wpdb->prefix."bp_future_checkins";
				$pid = $_POST['placeid'];
				$wpdb->delete( $tbl, array( 'id' => $pid ), array( '%d' ) );
				echo 'place-deleted';
				die;
            }
        }
    }
    new BpchkAjax();
}