<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

global $wpdb;
$tbl = $wpdb->prefix."bp_future_checkins";
$charset_collate = $wpdb->get_charset_collate();
if ($wpdb->get_var('SHOW TABLES LIKE '.$tbl) != $tbl) {
	$sql = "CREATE TABLE $tbl (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		place varchar(100) NOT NULL,
		visited varchar(3) NOT NULL,
		uid mediumint(9) NOT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";

	require_once( ABSPATH.'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}