<?php 
	global $wpdb;
	$tbl = $wpdb->prefix."bp_future_checkins";
	$qry = "SELECT * FROM $tbl WHERE `visited` = 'no' AND `uid` = ".get_current_user_id();
	$places = $wpdb->get_results( $qry );

	$all_places = '';
	if( !empty( $places ) ) {
		foreach( $places as $place ) {
			$all_places[] = $place->place;
		}
		$all_places = json_encode( $all_places );
	}
?>
<div class="add-place-block">
	<span class="bkchk-error" id="no-place-to-add">No Valid Place Found To Add.</span>
	<span class="bkchk-success" id="place-add-success">This place has been added successfully.</span>
	<span class="bkchk-error" id="place-alrdy-added">You've already added this place</span>
	<input type="text" placeholder="Add place..." id="bpchk-add-place-txt" class="place" autocomplete="on" onkeyup="search_place()">
	<input type="button" id="bpchk-add-place-btn" value="Add Place" class="bpchk-btn">
	<input type="hidden"  id="bpchk-all-added-places" value='<?php echo $all_places;?>'>
</div>

<h3>List of the places added to visit: </h3>

<div class="bpchk-places-added">
	<table class="bpchk-plces-tbl">
		<thead>
			<tr>
				<th width="70%">Place</th>
				<th width="20%">Actions</th>
			</tr>
		</thead>
		<tbody class="bpchk-places-to-visit-list">
			<?php if( empty( $places ) ) {?>
				<tr class="bpchk-no-place-added">
					<td colspan="3">No Places Added!</td>
				</tr>
			<?php } else {?>
				<?php foreach( $places as $place) {?>
					<tr id="place-<?php echo $place->id;?>">
						<td id="place-name-<?php echo $place->id;?>">
							<?php echo $place->place;?>
						</td>
						<td>
							<a href="javascript:void(0);" class="bpchk-visited-btn" data-placeid="<?php echo $place->id;?>" data-place="<?php echo $place->place;?>">
								<span title="Mark this place as visited.">
									<i class="fa fa-check" aria-hidden="true"></i>
								</span>
							</a>
							<a href="javascript:void(0);" class="bpchk-edit" data-placeid="<?php echo $place->id;?>">
								<span title="Edit this place">
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</span>
							</a>
							<a href="javascript:void(0);" class="bpchk-dlt" data-placeid="<?php echo $place->id;?>">
								<span title="Delete, if added this place by mistake!">
									<i class="fa fa-trash-o" aria-hidden="true"></i>
								</span>
							</a>
						</td>
					</tr>
					<tr class="place-inline-editor" id="bpchk-edit-place-<?php echo $place->id;?>">
						<td>
							<input type="text" placeholder="Add place..." class="place update-place" autocomplete="on" onkeyup="search_place()" id="new-place-<?php echo $place->id;?>" value="<?php echo $place->place?>">
						</td>
						<td>
							<input type="button" value="Update Place" class="bpchk-update-btn" data-placeid="<?php echo $place->id;?>">
							<input type="button" value="Cancel" class="bpchk-close-editor">
						</td>
					</tr>
				<?php }?>
			<?php }?>
		</tbody>
	</table>
</div>