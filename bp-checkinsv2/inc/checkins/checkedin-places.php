<?php 
global $wpdb;
$tbl = $wpdb->prefix."bp_future_checkins";
$qry = "SELECT * FROM $tbl WHERE `visited` = 'yes' AND `uid` = ".get_current_user_id();
$places = $wpdb->get_results( $qry );
?>
<div class="bpchk-places-added">
	<table class="bpchk-plces-tbl">
		<thead>
			<tr>
				<th width="10%">Sr. No.</th>
				<th width="90%">Place</th>
			</tr>
		</thead>
		<tbody class="bpchk-places-to-visit-list">
			<?php if( empty( $places ) ) {?>
				<tr>
					<td colspan="3">No Places Marked As Visited!</td>
				</tr>
			<?php } else {?>
				<?php $count = 0;?>
				<?php foreach( $places as $index => $place) {?>
					<tr>
						<td><?php echo ++$count;?></td>
						<td><?php echo $place->place;?></td>
					</tr>
				<?php }?>
			<?php }?>
		</tbody>
	</table>
</div>