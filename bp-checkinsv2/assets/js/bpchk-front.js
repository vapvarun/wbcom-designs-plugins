jQuery(document).ready(function(){
	//Add Place
	jQuery(document).on('click', '#bpchk-add-place-btn', function(){
		var add_place_ajax = true;
		var place = jQuery( '#bpchk-add-place-txt' ).val();
		var all_places = jQuery( '#bpchk-all-added-places' ).val();

		if( place == '' ) {
			jQuery( '#no-place-to-add' ).fadeIn('slow').delay(10000);
			jQuery( '#no-place-to-add' ).fadeOut('slow');
			add_place_ajax = false;
		} else if( all_places.length != 0 ) {
			all_places = JSON.parse( all_places );
			if (jQuery.inArray( place, all_places ) != -1) {
				jQuery( '#place-alrdy-added' ).fadeIn('slow').delay(10000);
				jQuery( '#place-alrdy-added' ).fadeOut('slow');
				add_place_ajax = false;
			} else {
				add_place_ajax = true;
			}
		} else {
			add_place_ajax = true;
		}

		if( add_place_ajax ) {
			jQuery( '#bpchk-add-place-btn' ).val( 'Adding..' );
			jQuery.post(
				bpchk_ajax_object.ajax_url,
				{
					'action' : 'bpchk_add_place_to_checkin',
					'place' : place,
					'all_places' : all_places,
				},
				function(response) {
					jQuery( '#bpchk-add-place-btn' ).val( 'Add Place' );
					var msg = response['msg'];
					var pid = response['pid'];
					if( msg == 'place-add-success' ){
						jQuery( '#place-add-success' ).fadeIn('slow').delay(10000);
						jQuery( '#place-add-success' ).fadeOut('slow');
						jQuery( '#bpchk-add-place-txt' ).val('').focus();

						var html = '';

						html += '<tr id="place-'+pid+'">';
						html += '<td id="place-name-'+pid+'">'+place+'</td>';
						html += '<td>';
						html += '<a href="javascript:void(0);" class="bpchk-visited-btn" data-placeid="'+pid+'" data-place="'+place+'">';
						html += '<span title="Mark this place as visited."><i class="fa fa-check" aria-hidden="true"></i></span>';
						html += '</a>';
						html += '<a href="javascript:void(0);" class="bpchk-edit" data-placeid="'+pid+'">';
						html += '<span title="Edit this place"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>';
						html += '</a>';
						html += '<a href="javascript:void(0);" class="bpchk-dlt" data-placeid="'+pid+'">';
						html += '<span title="Delete, if added this place by mistake!"><i class="fa fa-trash-o" aria-hidden="true"></i></span>';
						html += '</a>';
						html += '</td>';
						html += '</tr>';
						html += '<tr class="place-inline-editor" id="bpchk-edit-place-'+pid+'">';
						html += '<td>';
						html += '<input type="text" placeholder="Add place..." class="place update-place" autocomplete="on" onkeyup="search_place()" id="new-place-'+pid+'" value="'+place+'">';
						html += '</td>';
						html += '<td>';
						html += '<input type="button" value="Update Place" class="bpchk-update-btn" data-placeid="'+pid+'">';
						html += '<input type="button" value="Cancel" class="bpchk-close-editor">';
						html += '</td>';
						html += '</tr>';

						jQuery( html ).prependTo( '.bpchk-places-to-visit-list' );
						jQuery('.bpchk-no-place-added').hide();
						jQuery('#bpchk-all-added-places').val( response['all_places'] );
					}
				},
				"JSON"
			);
		}
	});

	//Mark a place as visited
	jQuery(document).on('click', '.bpchk-visited-btn', function() {
		var pid = jQuery( this ).data( 'placeid' );
		jQuery( this ).closest('tr').css( 'background-color', '#6CC417' );
		jQuery.post(
			bpchk_ajax_object.ajax_url,
			{
				'action' : 'bpchk_mark_place_visited',
				'placeid' : pid,
				'place' : jQuery( this ).data( 'place' ),
			},
			function(response) {
				if( response == 'place-marked-visited-success' ) {
					jQuery( '#place-'+pid ).remove();
				}
			}
		);
	});

	//Edit a place
	jQuery(document).on('click', '.bpchk-edit', function(){
		var pid = jQuery(this).data('placeid');
		jQuery('.place-inline-editor').hide();
		jQuery('#bpchk-edit-place-'+pid).show();
	});

	//Cancel Editing
	jQuery(document).on('click', '.bpchk-close-editor', function(){
		jQuery('.place-inline-editor').hide();
	});

	//Update place
	jQuery(document).on('click', '.bpchk-update-btn', function(){
		var pid = jQuery(this).data('placeid');
		var new_place = jQuery('#new-place-'+pid).val();
		jQuery(this).val('Updating..');
		jQuery.post(
			bpchk_ajax_object.ajax_url,
			{
				'action' : 'bpchk_update_place',
				'placeid' : pid,
				'place' : new_place,
			},
			function(response) {
				if( response == 'place-updated' ) {
					jQuery('#place-name-'+pid).html( new_place );
					jQuery('.place-inline-editor').hide();
				}
			}
		);
	});

	//Delete a place
	jQuery(document).on('click', '.bpchk-dlt', function(){
		var pid = jQuery( this ).data( 'placeid' );
		jQuery( this ).closest('tr').css( 'background-color', '#FF9999' );
		jQuery.post(
			bpchk_ajax_object.ajax_url,
			{
				'action' : 'bpchk_delete_place',
				'placeid' : pid,
			},
			function(response) {
				if( response == 'place-deleted' ) {
					jQuery( '#place-'+pid ).remove();
					jQuery( '#bpchk-edit-place-'+pid ).remove();
				}
			}
		);
	});
});

//Search place to add place
function search_place() {
	var options = {
		types: ['(regions)'],
	}
	var place_txt = document.getElementsByClassName('place');
	for( i = 0; i<place_txt.length; i++ ) {
		var autocomplete = new google.maps.places.Autocomplete(place_txt[i],options);
	}
}