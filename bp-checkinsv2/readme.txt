=== BP Checkins v2 ===
Contributors: wbcomdesigns
Donate link: https://wbcomdesigns.com/contact/
Tags: buddypress, checkins, profile menu
Requires at least: 3.0.1
Tested up to: 4.6.1
Stable tag: 4.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
 
This plugin allows buddypress members to add places that he wants to visit in near future and mark them as "visited" when he has visited them.
 
== Installation ==
 
1. Upload the entire bp-checkinsv2 folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
 
== Frequently Asked Questions ==
 
= How can we add a place to save as future checking? =
 
Just goto your member profile and you can see the "Checkins" tab. Click on it and then you will see a child tab "to checkin" to add a place that you want to visit.
 
== Screenshots ==

The screenshots are present in the root of the plugin folder.
1. screenshot-1 - is the screen that shows the user the places that he has marked as visited.
2. screenshot-2 - is the screen that shows the user an activity created on his profile activity stream that he has visited a place.
3. screenshot-3 - is the screen that allows user to add a checkin to make in near future and the corresponding list.