<div class="pg-heading">
	<img src="<?php echo plugin_dir_url( __FILE__ )."images/System-Status-bar.jpg";?>" atl="System Status">
</div>

<?php

ob_start();
include_once("includes/function.php");

$obj= new SystemStatus();

global $wpdb;

//Memory Limit
$memory = "-";
if (defined('WP_MEMORY_LIMIT')) {
  $memory = WP_MEMORY_LIMIT;
}

//WP DEBUG MODE
$debug = "-";
if (defined('WP_DEBUG')) {
  $debug = WP_DEBUG;
}

//WP CRON
$cron = "-";
if (defined('DISABLE_WP_CRON')) {
  $cron = DISABLE_WP_CRON;
}

$cron_arr = _get_cron_array();

$link = mysqli_connect( $wpdb->dbhost, $wpdb->dbuser, $wpdb->dbpassword ); //create connection link

//Server Software
$server_software = "-";
if ( isset( $_SERVER['SERVER_SOFTWARE'] ) ) {
  $server_software = $_SERVER['SERVER_SOFTWARE'];
}

//Server Name
$server_name = "-";
if ( isset( $_SERVER['HTTP_HOST'] ) ) {
  $server_name = $_SERVER['HTTP_HOST'];
}

//Current IP
$current_ip = "-";
if ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
  $current_ip = $_SERVER['REMOTE_ADDR'];
}

//cURL Info
$curl_ver = curl_version();
if( !empty( $curl_ver ) ){
  $curl = $curl_ver['version']." ".$curl_ver['ssl_version'];
} else {
    $curl = '-';
}

//Suhosin Installed
$suhosin = true;
if (!extension_loaded('suhosin')) {
    $suhosin = false;
}

$all_plugins = get_plugins();


$error=array();
$success='';

if(isset($_REQUEST['submit']))  /*If submit need to help form*/
{   
    

    if ($_REQUEST['customer_name']) {
        $name=$_REQUEST['customer_name'];
    }else{
        $error[]='Please Enter name !';
    }

    if ($_REQUEST['email']) {
        $email=$_REQUEST['email'];
    }else{
        $error[]='Please Enter email !';
    }

    if ($_REQUEST['message']) {
        $message=$_REQUEST['message'];
    }

    if (count($error)==0) {
        $obj->need_to_help_from($name,$email,$message);
        $success="Your query has been successfully sent .";
    }


}    


?>

<h1>Need Help</h1>
<?php if (!empty($success)) { ?>
<div class="updated settings-error notice is-dismissible"><?php echo $success;  ?></div>
<?php  } ?>

<?php if (count($error)>0) { 
    foreach ($error as $key => $value) { ?>
<div class="updated settings-error notice is-dismissible"><?php echo $value;  ?></div>
<?php    }  } ?>

<div class="status-wrap">
 <form method="POST">
  <table class="form-table">
         <tbody>
            <tr valign="top">
                <th scope="row">
                    <label for="num_elements">
                        Name :
                    </label> 
                </th>
                <td>
                   <input name="customer_name" type="text" value="" required> 
                    <br>
                                    
                </td>
              </tr>
              <tr valign="top">
                <th scope="row">
                    <label for="num_elements">
                        Email:
                    </label> 
                </th>
                <td>
                   <input name="email" type="email" value="" required>             
                </td>
              </tr>

              <tr valign="top">
                <th scope="row">
                    <label for="num_elements">
                        Message:
                    </label> 
                </th>
                <td>
                <textarea style="width: 18%;" name="message"></textarea>             
                </td>
              </tr>

            <tr valign="top">
                    <td colspan="2">
                    <input type="submit" name="submit" value="submit" class="button button-primary"></td>
            </tr>

            </tbody>
        </table>
    </form>
 </div>  


<!----------------------_________________Wordpress Environment------------------------------------------>

<div class= "status-wrap">
    <table>
      <thead>
        <tr>
          <th colspan="2">
            <h2>Wordpress Environment</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        <!--HOME URL-->
        <tr>
          <td><h4>Home URL:</h4></td>
          <td><?php echo home_url();?></td>
        </tr>
        <!--SITE URL-->
        <tr>
          <td><h4>Site URL:</h4></td>
          <td><?php echo get_site_url();?></td>
        </tr>
        <!--Wordpress Version-->
        <tr>
          <td><h4>Wordpress Version:</h4></td>
          <td><?php echo get_bloginfo('version');?></td>
        </tr>
        <!--Wordpress MultiSite-->
        <tr>
          <td><h4>Wordpress MultiSite:</h4></td>
          <?php if ( is_multisite() ){?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/symbol_check.png"?>"></td>
          <?php } else {?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/erase-icon.png"?>"></td>
          <?php }?>
        </tr>
        <!--Wordpress Memory Limit-->
        <tr>
          <td><h4>Wordpress Memory Limit:</h4></td>
          <td><?php echo $memory;?></td>
        </tr>
        <!--Wordpress Debug Mode-->
        <tr id="wp-debug-row">
          <td><h4>Wordpress Debug Mode:</h4></td>
          <?php if ( $debug ){?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/symbol_check.png"?>"></td>
            <td>
                <input type="button" class="button button-primary" id="turn-off-debug" value="Turn Off">
            </td>
          <?php } else {?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/erase-icon.png"?>"></td>
            <td>
                <input type="button" class="button button-primary" id="turn-on-debug" value="Turn On">
            </td>
          <?php }?>
        </tr>
        <!--Wordpress Cron-->
        <tr>
          <td><h4>Wordpress Cron:</h4></td>
          <?php if ( !empty( $cron_arr ) ){?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/symbol_check.png"?>"></td>
          <?php } else {?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/erase-icon.png"?>"></td>
          <?php }?>
        </tr>
        <!--Language-->
        <tr>
          <td><h4>Language:</h4></td>
          <td><?php echo get_locale();?></td>
        </tr>
      </tbody>
    </table>
</div>

<!--------------------------------Wordpress Database Environment------------------------------------------>


<div class="status-wrap">
    <table>
      <thead>
        <tr>
          <th colspan="2">
            <h2>Wordpress Database Environment</h2>
          </th>
        </tr>
      </thead>
      <tbody>
            <!--MySql Version-->
        <tr>
          <td><h4>MySql Version:</h4></td>
          <td><?php echo mysqli_get_server_info( $link );?></td>
        </tr>
            <!--WPDB Prefix-->
            <tr>
              <td><h4>WPDB Prefix:</h4></td>
              <td><?php echo $wpdb->prefix;?></td>
            </tr>
            <!--DB User-->
            <tr>
              <td><h4>DB User:</h4></td>
              <td><?php echo $wpdb->dbuser;?></td>
            </tr>
            <!--DB Password-->
            <tr>
              <td><h4>DB Password:</h4></td>
              <td><?php if( empty( $wpdb->dbpassword ) ) echo "No Password"; else echo $wpdb->dbpassword;?></td>
            </tr>
            <!--DB Name-->
            <tr>
              <td><h4>DB Name:</h4></td>
              <td><?php echo $wpdb->dbname;?></td>
            </tr>
            <!--DB Host-->
            <tr>
              <td><h4>DB Host:</h4></td>
              <td><?php echo $wpdb->dbhost;?></td>
            </tr>
      </tbody>
    </table>
</div>

<!------------------------------------Server Environment------------------------------------------>

<div class="status-wrap">
    <table>
      <thead>
        <tr>
          <th colspan="2">
            <h2>Server Environment</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        <!--Server Info-->
        <tr>
          <td><h4>Server Info:</h4></td>
          <td><?php echo $server_software;?></td>
        </tr>
        <!--Server Name-->
        <tr>
          <td><h4>Server Name:</h4></td>
          <td><?php echo $server_name;?></td>
        </tr>
        <!--Current IP-->
        <tr>
          <td><h4>Current IP:</h4></td>
          <td><?php echo $current_ip;?></td>
        </tr>
        <!--PHP Version-->
        <tr>
          <td><h4>PHP Version:</h4></td>
          <td><?php echo PHP_VERSION;?></td>
        </tr>
        <!--PHP Post Max Size-->
        <tr>
          <td><h4>PHP Post Max Size:</h4></td>
          <td><?php echo ini_get('post_max_size');?></td>
        </tr>
        <!--PHP Time Limit-->
        <tr>
          <td><h4>PHP Time Limit:</h4></td>
          <td><?php echo ini_get('max_execution_time');?></td>
        </tr>
        <!--PHP Max Input Vars-->
        <tr>
          <td><h4>PHP Max Input Vars:</h4></td>
          <td><?php echo ini_get('max_input_vars');?></td>
        </tr>
        <!--cURL Version-->
        <tr>
          <td><h4>cURL Version:</h4></td>
          <td><?php echo $curl;?></td>
        </tr>
        <!--SUHOSIN Installed-->
        <tr>
          <td><h4>SUHOSIN Installed:</h4></td>
          <?php if ( $suhosin ){?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/symbol_check.png"?>"></td>
          <?php } else {?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/erase-icon.png"?>"></td>
          <?php }?>
        </tr>
        <!--Max Upload Size-->
        <tr>
          <td><h4>Max Upload Size:</h4></td>
          <td><?php echo ini_get('upload_max_filesize');?></td>
        </tr>
        <!--Default Timezone is UTC-->
        <tr>
          <td><h4>Default Timezone is UTC:</h4></td>
          <?php if ( date_default_timezone_get() == 'UTC' ){?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/symbol_check.png"?>"></td>
          <?php } else {?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/erase-icon.png"?>"></td>
          <?php }?>
        </tr>
      </tbody>
    </table>
</div>

<!------------------------------------Plugins Environment------------------------------------------>

<div class="status-wrap">   
    <table>
      <thead>
        <tr>
          <th colspan="4">
            <h2>Plugins Environment</h2>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php foreach( $all_plugins as $index => $plugin ){?>
          <tr>
            <td><h4><?php echo $plugin['Name']?></h4></td>
            <td><i>(Plugin by:<?php echo $plugin['Author']?>)</i></td>
            <td><i>(Version:<?php echo $plugin['Version']?>)</i></td>
            <?php if( is_plugin_active( $index ) ){?>
              <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/symbol_check.png"?>"></td>
            <?php }?>
          </tr>
        <?php }?>
      </tbody>
    </table>
</div>