<?php
/*
  Plugin Name: System Status
  Plugin URI: https://wbcomdesigns.com/contact/
  Description: This plugin provides a summary of the server and wordpress details
  Version: 1.0.0
  Author: Wbcom Designs
  Author URI: http://wbcomdesigns.com
 */

//Settings link for this plugin
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'system_status_page_link' );
function system_status_page_link( $links ){
    $page_link = array(
        '<a href="'.admin_url('admin.php?page=system-status').'">System Status</a>'
    );
    return array_merge( $links, $page_link );
}

//Creating menu page
add_action('admin_menu', 'system_status_admin_page');
function system_status_admin_page() {
    add_menu_page( 'Status', 'System Status', 'manage_options', 'system-status', 'system_status_page' );
}
function system_status_page() {
    include 'system_status_admin_page.php';
}

//Turn On The Debug Mode
add_action( 'wp_ajax_turn_on_debug', 'prefix_ajax_turn_on_debug');
add_action( 'wp_ajax_nopriv_turn_on_debug', 'prefix_ajax_turn_on_debug');
function prefix_ajax_turn_on_debug() {
  if( isset( $_POST['action'] ) ) {
    $config_file = dirname( getcwd() )."/wp-config.php";
    $config_file_contents = file_get_contents( $config_file );
    $new_contents = str_replace( "define('WP_DEBUG', false)", "define('WP_DEBUG', true)", $config_file_contents );
    file_put_contents( $config_file, $new_contents );
    echo plugins_url();
    die;
  }
}

//Turn Off The Debug Mode
add_action( 'wp_ajax_turn_off_debug', 'prefix_ajax_turn_off_debug');
add_action( 'wp_ajax_nopriv_turn_off_debug', 'prefix_ajax_turn_off_debug');
function prefix_ajax_turn_off_debug() {
  if( isset( $_POST['action'] ) ) {
    $config_file = dirname( getcwd() )."/wp-config.php";
    $config_file_contents = file_get_contents( $config_file );
    $new_contents = str_replace( "define('WP_DEBUG', true)", "define('WP_DEBUG', false)", $config_file_contents );
    file_put_contents( $config_file, $new_contents );
    echo plugins_url();
    die;
  }
}

//Add Plugin Assets at admin init
add_action( 'admin_init', 'add_system_status_assets' );
function add_system_status_assets(){
  wp_enqueue_script( 'system-status-js', plugin_dir_url(__FILE__).'/js/system-status.js', array( 'jquery' ) );
  wp_enqueue_style( 'system-status-css', plugin_dir_url(__FILE__).'/css/system-status.css' );
}