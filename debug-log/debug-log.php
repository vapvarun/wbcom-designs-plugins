<?php
/*
  Plugin Name: Debug log
  Plugin URI: https://wbcomdesigns.com/contact/
  Description: This plugin provides a debug log details.
  Version: 1.0.0
  Author: Wbcom Designs
  Author URI: http://wbcomdesigns.com
 */

//Settings link for this plugin
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'debug_log_page_link' );
function debug_log_page_link( $links ){
    $page_link = array(
        '<a href="'.admin_url('admin.php?page=debug-log').'">Debug Log</a>'
    );
    return array_merge( $links, $page_link );
}

//Creating menu page
add_action('admin_menu', 'debug_log_admin_page');
function debug_log_admin_page() {
    add_menu_page( 'Debug', 'Debug Log', 'manage_options', 'debug-log', 'debug_log_page' );
}
function debug_log_page() {
    include 'debug_log_admin_page.php';
}

//Turn On The Debug Mode & Debug Log Mode
add_action( 'wp_ajax_turn_on_debug_log', 'prefix_ajax_turn_on_debug_log');
add_action( 'wp_ajax_nopriv_turn_on_debug_log', 'prefix_ajax_turn_on_debug_log');
function prefix_ajax_turn_on_debug_log() {
  if( isset( $_POST['action'] ) ) {
    $config_file = dirname( getcwd() )."/wp-config.php";
    $config_file_contents = file_get_contents( $config_file );

    $existing_str = "define('WP_DEBUG', false);";
    $new_str = "define('WP_DEBUG', true);\ndefine( 'WP_DEBUG_LOG', true );";

    if (defined('WP_DEBUG_LOG') && true === WP_DEBUG_LOG) {
       $new_contents = str_replace( "define('WP_DEBUG', false);","define('WP_DEBUG', true);", $config_file_contents );
       file_put_contents( $config_file, $new_contents );
    }else{
      $new_contents = str_replace( $existing_str, $new_str, $config_file_contents );
      file_put_contents( $config_file, $new_contents );
    }

    echo plugins_url();
    die;
  }
}

//Turn Off The Debug Mode & Debug Log Mode
add_action( 'wp_ajax_turn_off_debug_log', 'prefix_ajax_turn_off_debug_log');
add_action( 'wp_ajax_nopriv_turn_off_debug_log', 'prefix_ajax_turn_off_debug_log');
function prefix_ajax_turn_off_debug_log() {
  if( isset( $_POST['action'] ) ) {
    $config_file = dirname( getcwd() )."/wp-config.php";
    $config_file_contents = file_get_contents( $config_file );
    $new_contents = str_replace( "define('WP_DEBUG', true)", "define('WP_DEBUG', false)", $config_file_contents );
    file_put_contents( $config_file, $new_contents );
    echo plugins_url();
    die;
  }
}

//Add Plugin Assets at admin init
add_action( 'admin_init', 'add_debug_log_assets' );
function add_debug_log_assets(){
  wp_enqueue_script('debug-log-js', plugin_dir_url(__FILE__).'/js/debug-log.js', array( 'jquery' ) );
  wp_enqueue_style('debug-log-css', plugin_dir_url(__FILE__).'/css/debug-log.css' );
}