jQuery(document).ready(function(){

	jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
 
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });

	//Hide the processing text
	jQuery( '#processing-txt' ).hide();

	//Turn On The Debug Mode
	jQuery(document).on('click', '#turn-on-debug-log', function(){
		jQuery( '#processing-txt' ).show();
		jQuery.post(
			ajaxurl,
			{
				'action' : 'turn_on_debug_log'
			},
			function(response){

				//alert(response);

				//return false;

				var html = '';
				html += '<td><h4>Wordpress Debug Mode:</h4></td>';
				html += '<td><img class="tick-icon" src="'+response+'/system-status/images/symbol_check.png"></td>';
				html += '<td>';
				html += '<input type="button" class="button button-primary" id="turn-off-debug-log" value="Turn Off">';
				html += '</td>';

				jQuery( '#wp-debug-row' ).html( html );

				location.reload(true);

			}
		);
	});

	//Turn Off The Debug Mode
	jQuery(document).on('click', '#turn-off-debug-log', function(){
		jQuery.post(
			ajaxurl,
			{
				'action' : 'turn_off_debug_log'
			},
			function( response ){
				var html = '';
				html += '<td><h4>Wordpress Debug Mode:</h4></td>';
				html += '<td><img class="tick-icon" src="'+response+'/system-status/images/erase-icon.png"></td>';
				html += '<td>';
				html += '<input type="button" class="button button-primary" id="turn-on-debug-log" value="Turn On">';
				html += '</td>';

				jQuery( '#wp-debug-row' ).html( html );
			}
		);
	});
});