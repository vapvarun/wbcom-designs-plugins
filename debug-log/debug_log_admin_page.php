<?php
//WP DEBUG MODE
$debug = "-";
if (defined('WP_DEBUG')) {
  $debug = WP_DEBUG;
}

$debug_log_file_path=dirname( getcwd() )."/wp-content/debug.log";

?>

<h1>Debug Log</h1>

<!-- Action on debug log Mode -->
<div class= "status-wrap">
    <table>
      <tbody>
        <!--Wordpress Debug Mode-->
        <tr id="wp-debug-row">
          <td><h4>Wordpress Debug Mode:</h4></td>
          <?php if ( $debug ){?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/symbol_check.png"?>"></td>
            <td>
                <input type="button" class="button button-primary" id="turn-off-debug-log" value="Turn Off">
            </td>
          <?php } else {?>
            <td><img class="tick-icon" src="<?php echo plugins_url()."/system-status/images/erase-icon.png"?>"></td>
            <td>
                <input type="button" class="button button-primary" id="turn-on-debug-log" value="Turn On">
            </td>
          <?php }?>
        </tr>
     
      </tbody>
    </table>
</div>

<!-- display Error Log -->
<div class= "status-wrap">
 <?php 

 if (file_exists($debug_log_file_path)) {
  //$debug_log=file_get_contents($debug_log_file_path);
   //$file = fopen($debug_log_file_path, "r+") or exit("Unable to open file!");
    //Output a line of the file until the end is reached

   $file = file($debug_log_file_path);

   for ($i = max(0, count($file)-50); $i < count($file); $i++) {
            echo $file[$i]."<br>";
    }


   /* $i=1;
    while(!feof($file))
      {
          echo fgets($file, 4096)."<br>";
          //echo fgets($file). "<br>";
     $i++; } */
    //fclose($file);
 }else{
   echo 'Debug Log Empty';
 } ?>
</div>