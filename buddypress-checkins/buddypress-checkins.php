<?php
/*
Plugin Name: Buddypress Checkins
Plugin URI: https://wbcomdesigns.com/contact/
Description: This plugin allows you to update the current location of the user
Version: 1.0.0
Author: Wbcom Designs
Author URI: http://wbcomdesigns.com
*/

error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

//Check buddypress plugin on activation
register_activation_hook( __FILE__, 'buddypress_checkins_activate' );
function buddypress_checkins_activate(){
    //Check if buddypress plugin is active or not
    if( !in_array( 'buddypress/bp-loader.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
        //Buddypress plugin is inactive, hence deactivate this plugin
        deactivate_plugins( plugin_basename( __FILE__ ) );
        wp_die( 'This plugin requires <b>Buddypress</b> plugin to be installed and active. Return to <a href="'.admin_url('plugins.php').'">Plugins</a>' );
    }
}

//Settings link for this plugin
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'buddypress_checkins_settings_link' );
function buddypress_checkins_settings_link( $links ){
    $settings_link = array( '<a href="'.admin_url('admin.php?page=bp-checkins').'">Setup</a>' );
    return array_merge( $links, $settings_link );
}

//Register custom variables
add_action( 'wp_head', 'custom_variables_buddypress_checkins' );
function custom_variables_buddypress_checkins(){
    ?>
        <script type="text/javascript">
            var bp_checkins_ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
            var bp_checkins_siteurl = '<?php bloginfo('url'); ?>';
            var bp_checkins_plugin_url = '<?php echo plugin_dir_url( __FILE__ ); ?>';
        </script>
    <?php 
        if ( is_user_logged_in() ) {
            $usr_data = get_userdata( get_current_user_id() );
            wp_enqueue_style( 'buddypress-checkins-css', plugin_dir_url(__FILE__).'assets/css/buddypress-checkins.css' );
            //echo '<pre>'; print_r( $_SERVER['REQUEST_URI'] ); die("here");
            
            //if ( strpos($_SERVER['REQUEST_URI'], "/members/{$usr_data->data->user_login}/") !== false ) {
                wp_enqueue_script( 'buddypress-checkins-js', plugin_dir_url(__FILE__).'assets/js/buddypress-checkins.js', array('jquery'), '1.8' );
            //}
        }
}

add_action( 'admin_init', 'custom_admin_variables_buddypress_checkins' );
function custom_admin_variables_buddypress_checkins(){
    wp_enqueue_script( 'buddypress-checkins-js', plugin_dir_url(__FILE__).'assets/js/buddypress-admin-checkins.js', array('jquery') );
}

//Add menu page
add_action( 'admin_menu', 'admin_page_google_details' );
function admin_page_google_details(){
    add_menu_page( 'BP Checkins', 'BP Checkins', 'manage_options', 'bp-checkins', 'bp_checkins_admin_page' );
}
function bp_checkins_admin_page(){
    if( !current_user_can( 'manage_options' ) ){
        wp_die( __( 'You don\'t have enough pemissions to access this page' ) );
    }
    include_once 'bp-checkins-admin-page.php';
}

//Get Locations
add_action( 'wp_ajax_get_locations', 'prefix_ajax_get_locations' );
add_action( 'wp_ajax_nopriv_get_locations', 'prefix_ajax_get_locations' );
function prefix_ajax_get_locations(){
    if( isset( $_POST['action'] ) ){
        
        $lat = $_POST['latitude'];
        $lon = $_POST['longitude'];
        
        $radius = get_option( 'google_range', true);
        if( empty( $radius ) ){
            $radius = 100;
        }

        $api_key = get_option( 'google_api_key');
        $place_type = unserialize( get_option( 'google_place_types' ) );
        $type = implode(',', $place_type);
        $output = "json"; //OR it can be XML
        $parameters = "location=$lat,$lon&radius=$radius&type=$type&key=$api_key";
        $curl = curl_init();

        $curl_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$lon&radius=1000&type=$type&heading=false&title=false&key=$api_key";
		
        curl_setopt( $curl, CURLOPT_URL, $curl_url );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
        $location_data = curl_exec($curl);
        curl_close($curl);
		
        if( $location_data ){
            $locations = json_decode( $location_data );
			
            if( !empty( $locations->results ) ){
                $loc_arr = array();
				
                foreach( $locations->results as $index => $location ){
				//	$url = sprintf('https://www.google.com/maps/embed/v1/search?key=%s&q=%s', $api_key,  $location->name);
                    //print_r( $location ); die;
                    $arr = array(
                        'name' => $location->name,
                        'place_id' => $location->place_id,
                        'reference' => $location->reference,
                        'vicinity' => $location->vicinity,
                        'icon' => $location->icon,
						 //'url' => $url,
                    );
                    $loc_arr[] = $arr;
                }
                echo json_encode( $loc_arr );
                die;
            } else {
                echo "No Locations Found!";
                die;
            }
        } else {
            echo "Error in getting locations";
            die;
        }
    }
}

//Get Locations
add_action( 'wp_ajax_get_location_detail', 'prefix_ajax_get_location_detail' );
add_action( 'wp_ajax_nopriv_get_location_detail', 'prefix_ajax_get_location_detail' );
function prefix_ajax_get_location_detail(){
    if( isset( $_POST['action'] ) ){
        
        $place_id = $_POST['place_id'];
        $reference = $_POST['reference'];
        $api_key = get_option( 'google_api_key');
        $output = "json"; //OR it can be XML
        
      //  $parameters = "placeid=$place_id&radius=$radius&key=$api_key";
        
        $curl = curl_init();

        $curl_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$place_id&key=$api_key";

        curl_setopt( $curl, CURLOPT_URL, $curl_url );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
        $location = curl_exec($curl);
        curl_close($curl);

        if( $location ){
            $location_detail = json_decode( $location );
            
            if( !empty( $location_detail->result ) ){
                $href = 'http://maps.google.com/?q='.$location_detail->result->name;
                $html = "<a title='".$location_detail->result->name."' href='".$href."' target='_blank'>".$location_detail->result->name."</a>";
                $place = array(
                    'place_name' => $location_detail->result->name,
                    'place_id' => $place_id,
                    'place_reference' => $reference,
                );
                
                //Update options table to save the checked in data temporarily
                global $wpdb;
                $tbl = $wpdb->prefix."options";
                $qry = "SELECT `option_id`,`option_value` from $tbl where `option_name` = 'temp_location'";
                $result = $wpdb->get_results( $qry );
                //If this temp location is empty, the first time
                if( empty( $result ) ){
                    //Insert this location
                    $wpdb->insert( 
                        $tbl, 
                        array( 
                            'option_name' => 'temp_location', 
                            'option_value' => serialize( $place ), 
                        ), 
                        array( 
                            '%s', 
                            '%s' 
                        ) 
                    );
                } else {
                    //Update this location
                    $option_id = $result[0]->option_id;
                    $wpdb->update( 
                        $tbl, 
                        array( 'option_value' => serialize( $place ) ), 
                        array( 'option_id' => $option_id ), 
                        array( '%s' ), 
                        array( '%d' ) 
                    );
                }
                
                echo $html;
                die;
            } else {
                echo "No Locations Found!";
                die;
            }
        } else {
            echo "Error in getting location details!";
            die;
        }
    }
}

//Update activity meta on post update
add_action( 'bp_activity_posted_update', 'update_meta_on_post_update', 10, 3 );
function update_meta_on_post_update( $content, $user_id, $activity_id ){
    global $wpdb;
    $tbl = $wpdb->prefix."options";
    $qry = "SELECT `option_value` from $tbl where `option_name` = 'temp_location'";
    $result = $wpdb->get_results( $qry );
    if( !empty( $result ) ){
        $place = $result[0]->option_value;
        bp_activity_update_meta( $activity_id, 'checkedin_location', $place );
        
        //Delete the entry from options table
        $wpdb->query( 
            $wpdb->prepare( 
                "DELETE FROM $tbl
                 WHERE option_name = %s",
                'temp_location' 
                )
        );
    }
    
    //Update content on post update
    $place = bp_activity_get_meta( $activity_id, 'checkedin_location', true );
    $unserialized_place = unserialize( $place );
    
    if( !empty( $unserialized_place ) ){
        $location = $unserialized_place['place_name'];
        $location_html = '-at <a class="checkin-loc" href="http://maps.google.com/?q='.$location.'" target="_blank" title="'.$location.'">'.$location.'</a>';

        //Get the content of the activity
        $activity_tbl = $wpdb->prefix."bp_activity";
        $activity_qry = "SELECT * from $activity_tbl where `id` = $activity_id";
        $activity_result = $wpdb->get_results( $activity_qry );

        $pos = strpos( $activity_result[0]->content, '-at <a class="checkin-loc"' );

        if( $pos == '' ){
            $wpdb->update( 
                $activity_tbl, 
                array( 'content' => $activity_result[0]->content.$location_html ), 
                array( 'id' => $activity_id ), 
                array( '%s' ), 
                array( '%d' ) 
            );
        }
    }
    
}

//Show the checked in location in activities
add_filter( 'bp_ajax_querystring', 'checked_activities_qs_filter', 111 );
function checked_activities_qs_filter( $qs ){
    global $bp, $buddyboss_ajax_qs;
    $buddyboss_ajax_qs = $qs;
    $action = $bp->current_action;
    
    if ( $action != "just-me" ) {
        // if we're on a different page than wall pass qs as is
        return $qs;
    }
    
    $activities = get_checked_in_activities();
    if ( ! $activities ) {
        add_filter( 'bp_has_activities', 'checked_in_activities_cancel_bp_has_activities' );
    }
    $nqs = "include=$activities";
    return $nqs;
}

//Get Checked In Activities
function get_checked_in_activities( $page = 0, $per_page=20 ) {
    global $bp, $wpdb, $buddyboss_ajax_qs;
    if (isset($bp->loggedin_user) && isset($bp->loggedin_user->id) && $bp->displayed_user->id == $bp->loggedin_user->id) {
        $myprofile = true;
    } else {
        $myprofile = false;
    }
    $user_id = $bp->displayed_user->id;
    $user_filter = $bp->displayed_user->domain;
    
    $tbl1 = bp_core_get_table_prefix() . 'bp_activity';
    $tbl2 = bp_core_get_table_prefix() . 'bp_activity_meta';
    
    $qry = "SELECT `id` from $tbl1";
    $activities = $wpdb->get_results( $qry );
    $tmp = array();
    
    foreach( $activities as $activity ){
        $activity_id = $activity->id;
        
        //Get location from activity id
        $meta_qry = "SELECT `meta_value` from $tbl2 where `activity_id` = $activity_id AND `meta_key` = 'checkedin_location'";
        $meta_result = $wpdb->get_results( $meta_qry );

        if( empty( $meta_result ) ){
        } else {
            $meta_location = unserialize( unserialize( $meta_result[0]->meta_value ) );
            
            $location = $meta_location['place_name'];
            $location_html = '-at <a class="checkin-loc" href="http://maps.google.com/?q='.$location.'" target="_blank" title="'.$location.'">'.$location.'</a>';

            //Get the content of the activity
            $activity_qry = "SELECT * from $tbl1 where `id` = $activity_id";
            $activity_result = $wpdb->get_results( $activity_qry );
            
            $pos = strpos( $activity_result[0]->content, '-at <a class="checkin-loc"' );
            
            if( $pos == '' ){
                $wpdb->update( 
                    $tbl1, 
                    array( 'content' => $activity_result[0]->content.$location_html ), 
                    array( 'id' => $activity_id ), 
                    array( '%s' ), 
                    array( '%d' ) 
                );
            }
        }
        $tmp[] = $activity_id;
    }
    
    $activity_list = implode( ",", $tmp );
    return $activity_list;
}

function checked_in_activities_cancel_bp_has_activities() {
    return false;
}
?>