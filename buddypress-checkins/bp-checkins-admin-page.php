<?php 
    $saved_api = '';
    $saved_range = '';
    $saved_place_types = '';
    //Save Deails
    if(isset($_POST['save_details'])){
        $api = $_POST['api_key'];
        $range = $_POST['range'] * 1000;
        $place_types = $_POST['place_types'];

        update_option('google_api_key', $api);
        update_option('google_range', $range);
        update_option('google_place_types', serialize( $place_types ) );
        
        echo '<div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Google Account Details Saved.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
    }

    //Remove Deails
    if(isset($_POST['remove_details'])){
        
        delete_option( 'google_api_key' );
        delete_option( 'google_range' );
        delete_option( 'google_place_types' );
        
        echo '<div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Google Account Details Removed.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
    }

    if( get_option( 'google_api_key') )
        $saved_api = get_option( 'google_api_key');
    if( get_option( 'google_range') )
        $saved_range = get_option( 'google_range') / 1000;
    if( get_option( 'google_place_types') )
        $saved_place_types = unserialize( get_option( 'google_place_types') );
?>



<div class="wrap">
    
    <p>
        <?php $img = plugin_dir_url(__FILE__).'/assets/images/Google-Maps-icon.png';?>
        <img src="<?php echo $img;?>" alt="Google Places Logo" title="Google Places" style="vertical-align: top; height: 175px;">
    </p>
    
    <h1>Google Account Details</h1>
    <form action="" method="post">
        <table class="form-table">
            <tbody>
                <!-- API Key -->
                <tr>
                    <th scope="row"><label for="api_key">API KEY</label></th>
                    <td><input type="text" class="regular-text" name="api_key" placeholder="API KEY" value="<?php echo $saved_api;?>"></td>
                </tr>
                
                <!-- Range -->
                <tr>
                    <th scope="row"><label for="range">Range</label></th>
                    <td>
                        <input value="<?php echo $saved_range;?>" id="google_range" type="range" class="regular-text" name="distance" min="0" max="10">
                    </td>
                    <td>
                        <span id="range_disp">
                            <?php if( $saved_range ) echo "$saved_range kms.";?>
                        </span>
                        <input type="hidden" name="range" value="" id="hidden_range">
                    </td>
                </tr>
                
                <!-- Types -->
                <tr>
                    <th scope="row"><label for="place_types">Place Types</label></th>
                    <td>
                        <p><input type="checkbox" <?php if( in_array('accounting', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="accounting">Accounting</p>
                        <p><input type="checkbox" <?php if( in_array('airport', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="airport">Airport</p>
                        <p><input type="checkbox" <?php if( in_array('amusement_park', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="amusement_park">Amusement Park</p>
                        <p><input type="checkbox" <?php if( in_array('aquarium', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="aquarium">Aquarium</p>
                        <p><input type="checkbox" <?php if( in_array('art_gallery', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="art_gallery">Art Gallery</p>
                        <p><input type="checkbox" <?php if( in_array('atm', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="atm">ATM</p>
                        <p><input type="checkbox" <?php if( in_array('bakery', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="bakery">Bakery</p>
                        <p><input type="checkbox" <?php if( in_array('bank', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="bank">Bank</p>
                        <p><input type="checkbox" <?php if( in_array('bar', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="bar">Bar</p>
                        <p><input type="checkbox" <?php if( in_array('beauty_salon', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="beauty_salon">Beauty Salon</p>
                        <p><input type="checkbox" <?php if( in_array('bicycle_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="bicycle_store">Bicycle Store</p>
                        <p><input type="checkbox" <?php if( in_array('book_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="book_store">Book Store</p>
                        <p><input type="checkbox" <?php if( in_array('bowling_alley', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="bowling_alley">Bowling Alley</p>
                        <p><input type="checkbox" <?php if( in_array('bus_station', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="bus_station">Bus Station</p>
                        <p><input type="checkbox" <?php if( in_array('cafe', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="cafe">Cafe</p>
                        <p><input type="checkbox" <?php if( in_array('campground', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="campground">Campground</p>
                        <p><input type="checkbox" <?php if( in_array('car_dealer', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="car_dealer">Car Dealer</p>
                        <p><input type="checkbox" <?php if( in_array('car_rental', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="car_rental">Car Rental</p>
                        <p><input type="checkbox" <?php if( in_array('car_repair', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="car_repair">Car Repair</p>
                        <p><input type="checkbox" <?php if( in_array('car_wash', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="car_wash">Car Wash</p>
                        <p><input type="checkbox" <?php if( in_array('casino', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="casino">Casino</p>
                        <p><input type="checkbox" <?php if( in_array('cemetery', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="cemetery">Cemetery</p>
                        <p><input type="checkbox" <?php if( in_array('church', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="church">Church</p>
                        <p><input type="checkbox" <?php if( in_array('city_hall', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="city_hall">City Hall</p>
                        <p><input type="checkbox" <?php if( in_array('clothing_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="clothing_store">Clothing Store</p>
                        <p><input type="checkbox" <?php if( in_array('convenience_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="convenience_store">Convenience Store</p>
                        <p><input type="checkbox" <?php if( in_array('courthouse', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="courthouse">Courthouse</p>
                        <p><input type="checkbox" <?php if( in_array('dentist', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="dentist">Dentist</p>
                        <p><input type="checkbox" <?php if( in_array('department_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="department_store">Department Store</p>
                        <p><input type="checkbox" <?php if( in_array('doctor', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="doctor">Doctor</p>
                        <p><input type="checkbox" <?php if( in_array('electrician', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="electrician">Electrician</p>
                        <p><input type="checkbox" <?php if( in_array('electronics_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="electronics_store">Electronics Store</p>
                        <p><input type="checkbox" <?php if( in_array('embassy', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="embassy">Embassy</p>
                        <p><input type="checkbox" <?php if( in_array('fire_station', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="fire_station">Fire Station</p>
                        <p><input type="checkbox" <?php if( in_array('florist', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="florist">Florist</p>
                        <p><input type="checkbox" <?php if( in_array('funeral_home', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="funeral_home">Funeral Home</p>
                        <p><input type="checkbox" <?php if( in_array('furniture_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="furniture_store">Furniture Home</p>
                        <p><input type="checkbox" <?php if( in_array('gas_station', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="gas_station">Gas Station</p>
                        <p><input type="checkbox" <?php if( in_array('gym', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="gym">Gym</p>
                        <p><input type="checkbox" <?php if( in_array('hair_care', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="hair_care">Hair Care</p>
                        <p><input type="checkbox" <?php if( in_array('hardware_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="hardware_store">Hardware Store</p>
                        <p><input type="checkbox" <?php if( in_array('hindu_temple', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="hindu_temple">Hindu Temple</p>
                        <p><input type="checkbox" <?php if( in_array('home_goods_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="home_goods_store">Home Goods Store</p>
                    </td>
                    <td>
                        <p><input type="checkbox" <?php if( in_array('hospital', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="hospital">Hospital</p>
                        <p><input type="checkbox" <?php if( in_array('insurance_agency', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="insurance_agency">Insurance Agency</p>
                        <p><input type="checkbox" <?php if( in_array('jewelry_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="jewelry_store">Jewelery Store</p>
                        <p><input type="checkbox" <?php if( in_array('laundry', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="laundry">Laundry</p>
                        <p><input type="checkbox" <?php if( in_array('lawyer', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="lawyer">Lawyer</p>
                        <p><input type="checkbox" <?php if( in_array('library', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="library">Library</p>
                        <p><input type="checkbox" <?php if( in_array('liquor_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="liquor_store">Liquor Store</p>
                        <p><input type="checkbox" <?php if( in_array('local_government_office', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="local_government_office">Local Govt. Office</p>
                        <p><input type="checkbox" <?php if( in_array('locksmith', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="locksmith">Locksmith</p>
                        <p><input type="checkbox" <?php if( in_array('lodging', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="lodging">Lodging</p>
                        <p><input type="checkbox" <?php if( in_array('meal_delivery', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="meal_delivery">Meal Delivery</p>
                        <p><input type="checkbox" <?php if( in_array('meal_takeaway', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="meal_takeaway">Meal Takeaway</p>
                        <p><input type="checkbox" <?php if( in_array('mosque', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="mosque">Mosque</p>
                        <p><input type="checkbox" <?php if( in_array('movie_rental', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="movie_rental">Movie Rental</p>
                        <p><input type="checkbox" <?php if( in_array('movie_theater', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="movie_theater">Movie Theater</p>
                        <p><input type="checkbox" <?php if( in_array('moving_company', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="moving_company">Moving Company</p>
                        <p><input type="checkbox" <?php if( in_array('museum', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="museum">Museum</p>
                        <p><input type="checkbox" <?php if( in_array('night_club', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="night_club">Night Club</p>
                        <p><input type="checkbox" <?php if( in_array('painter', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="painter">Painter</p>
                        <p><input type="checkbox" <?php if( in_array('park', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="park">Park</p>
                        <p><input type="checkbox" <?php if( in_array('parking', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="parking">Parking</p>
                        <p><input type="checkbox" <?php if( in_array('pet_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="pet_store">Pet Store</p>
                        <p><input type="checkbox" <?php if( in_array('pharmacy', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="pharmacy">Pharmacy</p>
                        <p><input type="checkbox" <?php if( in_array('physiotherapist', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="physiotherapist">Physiotherapist</p>
                        <p><input type="checkbox" <?php if( in_array('plumber', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="plumber">Plumber</p>
                        <p><input type="checkbox" <?php if( in_array('police', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="police">Police</p>
                        <p><input type="checkbox" <?php if( in_array('post_office', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="post_office">Post Ofice</p>
                        <p><input type="checkbox" <?php if( in_array('real_estate_agency', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="real_estate_agency">Real Estate Agency</p>
                        <p><input type="checkbox" <?php if( in_array('restaurant', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="restaurant">Restaurant</p>
                        <p><input type="checkbox" <?php if( in_array('roofing_contractor', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="roofing_contractor">Roofing Contractor</p>
                        <p><input type="checkbox" <?php if( in_array('rv_park', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="rv_park">RV Park</p>
                        <p><input type="checkbox" <?php if( in_array('school', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="school">School</p>
                        <p><input type="checkbox" <?php if( in_array('shoe_store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="shoe_store">Shoe Store</p>
                        <p><input type="checkbox" <?php if( in_array('shopping_mall', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="shopping_mall">Shopping Mall</p>
                        <p><input type="checkbox" <?php if( in_array('spa', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="spa">Spa</p>
                        <p><input type="checkbox" <?php if( in_array('stadium', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="stadium">Stadium</p>
                        <p><input type="checkbox" <?php if( in_array('storage', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="storage">Storage</p>
                        <p><input type="checkbox" <?php if( in_array('store', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="store">Store</p>
                        <p><input type="checkbox" <?php if( in_array('subway_station', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="subway_station">Subway Station</p>
                        <p><input type="checkbox" <?php if( in_array('synagogue', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="synagogue">Synagogue</p>
                        <p><input type="checkbox" <?php if( in_array('taxi_stand', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="taxi_stand">Taxi Stand</p>
                        <p><input type="checkbox" <?php if( in_array('train_station', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="train_station">Train Station</p>
                        <p><input type="checkbox" <?php if( in_array('transit_station', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="transit_station">Transit Station</p>
                        <p><input type="checkbox" <?php if( in_array('travel_agency', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="travel_agency">Travel Agency</p>
                        <p><input type="checkbox" <?php if( in_array('university', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="university">University</p>
                        <p><input type="checkbox" <?php if( in_array('veterinary_care', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="veterinary_care">Veterinary Care</p>
                        <p><input type="checkbox" <?php if( in_array('zoo', $saved_place_types) ) echo "checked='checked'";?> name="place_types[]" value="zoo">Zoo</p>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <p class="submit">
            <input type="submit" value="Save Details" class="button button-primary" name="save_details">
            <?php if($saved_api){?>
                <input type="submit" value="Remove Details" class="button button-primary" name="remove_details">
            <?php }?>
        </p>
    </form>
</div>