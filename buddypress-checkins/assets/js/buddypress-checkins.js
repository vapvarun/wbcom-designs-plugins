jQuery(document).ready(function(){
    
    jQuery("#whats-new-textarea").append('<a href="javascript:void(0);" id="position-me" title="Checkin here!"><img src="'+bp_checkins_plugin_url+'/assets/images/checkin.png" id="checkin-img"></a><p class="wait-text"><i>Please Wait...</i></p><div class="locations" style="display: none; margin-top: 10px !important;"><ul id="locations-list" style="margin-left: 0 !important;"></ul></div>');
    
    //Append a division under the text area to store the check in location
    jQuery('<div class="checkin-panel"></div>').insertBefore('#position-me');
    
    
    //Click icon to getnearby places
    jQuery(document).on("click", "#position-me", function(){
        navigator.geolocation.getCurrentPosition(function success(pos) {
            var crd = pos.coords;
            jQuery( ".wait-text" ).show();
            jQuery.post(
                bp_checkins_ajaxurl,
                {
                    'action' : 'get_locations',
                    'latitude' : crd.latitude,
                    'longitude' : crd.longitude,
                },
                function( response ){
                    jQuery( ".wait-text" ).hide();
                    jQuery( ".locations" ).show();
                    var html = '';
                    
                    for( i in response ){
                        var div_html = '';
                        
                        div_html += '<div>';
                        div_html += '<img height="25px" width="25px" title="'+response[i]['name']+'" src="'+response[i]['icon']+'">';
                        div_html += '<div class="loc-title">';
                        div_html += '<h3>'+response[i]['name']+'</h3>';
                        div_html += '<span>'+response[i]['vicinity']+'</span>';
                        div_html += '</div>';
                        div_html += '</div>';
                        
                        html += '<li class="single-location" data-reference="'+response[i]['reference']+'" id="'+response[i]['place_id']+'">';
                        html += div_html;
                        html += '</li>';
                    }
                    jQuery( "#locations-list" ).html(html)
                },
                "JSON"
            );
        });
    });
    
    //Get place details
    jQuery(document).on("click", ".single-location", function(){
        var place_id = jQuery(this).attr("id");
        var reference = jQuery(this).data('reference');
        jQuery( ".wait-text" ).html( "<i>Checking in...</i>" );
        jQuery( ".wait-text" ).show();
        jQuery.post(
            bp_checkins_ajaxurl,
            {
                'action' : 'get_location_detail',
                'place_id' : place_id,
                'reference' : reference,
            },
            function( response ){
                jQuery(".checkin-panel").html( "-at "+response );
                jQuery( ".wait-text" ).hide();
                jQuery( ".locations" ).hide();
            }
        );
    });
    
    //Range slider on admin page
    jQuery( '#google_range' ).change(function(){
        var range = jQuery( this ).val();
        jQuery( '#range_disp' ).html( range+" kms." );
        jQuery( '#hidden_range' ).val( range );
        console.log( jQuery( '#hidden_range' ).val() );
    });
});