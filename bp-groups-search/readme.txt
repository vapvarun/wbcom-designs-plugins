=== Buddypress Groups Search ===
Contributors: wbcomdesigns
Donate link: https://wbcomdesigns.com/contact/
Tags: buddypress, groups
Requires at least: 3.0.1
Tested up to: 4.6.1
Stable tag: 4.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
 
This plugin modifies the search template that is on the "/groups" page. It adds a feature of "group types" that are involved while searching groups. Now the users can search groups based on group types.
 
== Installation ==
 
1. Upload the entire "bp-groups-search" folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
 
== Frequently Asked Questions ==
 
= How can we search the groups by using this plugin? =
 
Simply goto the groups pafe from site front, you can see the search bar appearing above the groups listing. There you can see the "select" box that lists all the group types on the site. The attached screenshot will help you better understand the purpose. The screenshot is within the plugin folder.
 
== Screenshots ==

The screenshots are present in the root of the plugin folder.
1. screenshot-1 - is the screen that shows the modified search template on buddypress groups page.