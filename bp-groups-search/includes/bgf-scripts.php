<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Class to add custom scripts and styles
 */
if( !class_exists( 'BGFScriptsStyles' ) ) {
	class BGFScriptsStyles{

		/**
		 * Constructor
		 */
		function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'bgf_custom_variables' ) );
		}

		/**
		 * Actions performed for enqueuing scripts and styles for site front
		 */
		function bgf_custom_variables() {
			wp_enqueue_script('bgf-js-front',BGF_PLUGIN_URL.'assets/js/bgf-front.js', array('jquery'));
			wp_enqueue_style('bgf-front-css', BGF_PLUGIN_URL.'assets/css/bgf-front.css');

			//Font Awesome
			wp_enqueue_style('bgf-fa-css', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
		}
	}
	new BGFScriptsStyles();
}